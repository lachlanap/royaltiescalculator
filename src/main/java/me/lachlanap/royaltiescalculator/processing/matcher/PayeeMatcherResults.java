package me.lachlanap.royaltiescalculator.processing.matcher;

import java.util.List;
import java.util.Objects;
import me.lachlanap.royaltiescalculator.data.Payee;
import me.lachlanap.royaltiescalculator.processing.SalesData;

/**
 *
 * @author lachlan
 */
public class PayeeMatcherResults {

    private final List<SalesData.Item> unmatched;
    private final List<PayeeResult> results;

    public PayeeMatcherResults(List<SalesData.Item> unmatched, List<PayeeResult> results) {
        this.unmatched = unmatched;
        this.results = results;
    }

    public int getUnmatchedCount() {
        return unmatched.size();
    }

    public boolean allMatched() {
        return unmatched.isEmpty();
    }

    public List<SalesData.Item> getUnmatched() {
        return unmatched;
    }

    public List<PayeeResult> getPayeeResults() {
        return results;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("[PayeeMatcherResults: ")
                .append(getUnmatchedCount())
                .append(" unmatched items.");

        for (PayeeResult result : results) {
            builder.append("\n")
                    .append(result);
        }

        builder.append("]");

        return builder.toString();
    }

    public static class PayeeResult {

        private final Payee payee;
        private final List<Item> items;

        public PayeeResult(Payee payee, List<Item> items) {
            this.payee = payee;
            this.items = items;
        }

        public Payee getPayee() {
            return payee;
        }

        public List<Item> getItems() {
            return items;
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append(payee)
                    .append(": ");

            for (Item item : items) {
                builder.append("\n  ")
                        .append(item);
            }

            builder.append("\n");

            return builder.toString();
        }

    }

    public static class Item {

        private final SalesData.Item item;
        private final int percentRoyalty;
        private final double rrp;

        public Item(SalesData.Item item, int percentRoyalty, double rrp) {
            this.item = item;
            this.percentRoyalty = percentRoyalty;
            this.rrp = rrp;
        }

        public SalesData.Item getItem() {
            return item;
        }

        public int getPercentRoyalty() {
            return percentRoyalty;
        }

        public double getRRP() {
            return rrp;
        }

        @Override
        public String toString() {
            return item + " " + percentRoyalty + "%";
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 23 * hash + Objects.hashCode(this.item);
            hash = 23 * hash + this.percentRoyalty;
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            final Item other = (Item) obj;
            if (!Objects.equals(this.item, other.item))
                return false;
            if (this.percentRoyalty != other.percentRoyalty)
                return false;
            return true;
        }


    }
}
