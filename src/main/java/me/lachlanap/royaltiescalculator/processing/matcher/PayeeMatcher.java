package me.lachlanap.royaltiescalculator.processing.matcher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import me.lachlanap.royaltiescalculator.data.MatcherRule;
import me.lachlanap.royaltiescalculator.data.Payee;
import me.lachlanap.royaltiescalculator.database.Database;
import me.lachlanap.royaltiescalculator.processing.SalesData;
import me.lachlanap.royaltiescalculator.processing.matcher.PayeeMatcherResults.PayeeResult;

/**
 *
 * @author lachlan
 */
public class PayeeMatcher {

    private final Database database;
    private Future<List<MatcherRule>> rulesFuture;

    public PayeeMatcher(Database database) {
        this.database = database;
    }

    public void loadRules() {
        rulesFuture = database.getMatcherRules();
    }

    public PayeeMatcherResults match(SalesData salesData) throws ExecutionException, InterruptedException {
        List<MatcherRule> rules = database.getMatcherRules().get();


        /** Pair items with a payee */
        List<SalesData.Item> unmatched = new ArrayList<>();
        Map<Payee, List<PayeeMatcherResults.Item>> runningResults = new HashMap<>();

        for (MatcherRule rule : rules) {
            Payee payee = rule.getPayee();
            if (!runningResults.containsKey(payee))
                runningResults.put(payee, new ArrayList<PayeeMatcherResults.Item>());
        }

        for (SalesData.Item item : salesData.getItems())
            matchItem(rules, item, runningResults, unmatched);


        /** Convert payee-item results into PayeeResult objects */
        List<PayeeMatcherResults.PayeeResult> results = new ArrayList<>();
        for (Map.Entry<Payee, List<PayeeMatcherResults.Item>> entry : runningResults.entrySet()) {
            results.add(new PayeeResult(entry.getKey(), entry.getValue()));
        }

        return new PayeeMatcherResults(unmatched, results);
    }

    private void matchItem(List<MatcherRule> rules,
                           SalesData.Item item,
                           Map<Payee, List<PayeeMatcherResults.Item>> runningResults,
                           List<SalesData.Item> unmatched) {
        boolean matched = false;
        for (MatcherRule rule : rules) {

            if (rule.matches(item)) {
                Payee payee = rule.getPayee();
                runningResults.get(payee).add(new PayeeMatcherResults.Item(item,
                                                                           rule.getPercentRoyalty(),
                                                                           rule.getRRP()));

                matched = true;
            }
        }

        if (!matched)
            unmatched.add(item);
    }

}
