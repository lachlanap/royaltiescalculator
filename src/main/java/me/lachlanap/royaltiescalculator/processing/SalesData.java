package me.lachlanap.royaltiescalculator.processing;

import java.util.*;

/**
 *
 * @author lachlan
 */
public class SalesData {

    private final Set<Item> items;

    public SalesData() {
        items = new TreeSet<>();
    }

    private SalesData(Set<Item> items) {
        this.items = items;
    }

    public SalesData addItem(Item item) {
        Item insert = merge(item);
        Set<Item> mod;

        if (items.contains(item)) {
            mod = new TreeSet<>(items);
            mod.remove(item);
        } else {
            mod = this.items;
        }
        mod.add(insert);

        SalesData sd = new SalesData(mod);
        return sd;
    }

    public SalesData append(SalesData parsed) {
        Map<String, Item> tmp = new HashMap<>();

        for (Item i : items)
            tmp.put(i.sku, i);

        for (Item i : parsed.items) {
            if (tmp.containsKey(i.sku)) {
                Item a = tmp.get(i.sku);
                Item b = i;
                Item c = a.merge(b);

                tmp.put(c.sku, c);
            } else
                tmp.put(i.sku, i);
        }


        Set<Item> mod = new TreeSet<>();
        for (Item i : tmp.values())
            mod.add(i);

        SalesData sd = new SalesData(mod);
        return sd;
    }

    private Item merge(Item b) {
        if (items.contains(b))
            for (Item a : items)
                if (a.equals(b))
                    return a.merge(b);

        return b;
    }

    public Set<Item> getItems() {
        return items;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final SalesData other = (SalesData) obj;
        return items.containsAll(other.items) && other.items.containsAll(items);
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder("[SalesData ");
        for (Item i : items)
            b.append(i).append('\n');
        b.append(']');
        return b.toString();
    }

    public static class Item implements Comparable<Item> {

        private final String sku;
        private final String name;
        private final int qtyOrdered;
        private final double sales;

        public Item(String sku, String name, int qtyOrdered, double sales) {
            this.sku = sku;
            this.name = name;
            this.qtyOrdered = qtyOrdered;
            this.sales = sales;
        }

        public String getSku() {
            return sku;
        }

        public String getName() {
            return name;
        }

        public int getQtyOrdered() {
            return qtyOrdered;
        }

        public double getSales() {
            return sales;
        }

        public Item merge(Item b) {
            return new Item(sku, name, qtyOrdered + b.qtyOrdered, sales + b.sales);
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 23 * hash + Objects.hashCode(sku);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            final Item other = (Item) obj;
            return sku.equals(other.sku);
        }

        @Override
        public int compareTo(Item o) {
            return sku.compareTo(o.sku);
        }

        @Override
        public String toString() {
            return "[" + sku + ": " + name + "; " + qtyOrdered + " = " + sales + "]";
        }

    }
}
