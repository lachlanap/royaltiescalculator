package me.lachlanap.royaltiescalculator.processing.report;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.docbag.table.Cell;
import org.docbag.table.Row;
import org.docbag.table.Table;

/**
 *
 * @author lachlan
 */
public class Helpers {

    private static final NumberFormat currencyFormat;

    static {
        currencyFormat = NumberFormat.getNumberInstance();
        currencyFormat.setMinimumFractionDigits(2);
        currencyFormat.setMaximumFractionDigits(2);
    }

    public static String formatCurrency(double dollarValue) {
        synchronized (currencyFormat) {
            return "$" + currencyFormat.format(dollarValue);
        }
    }


    public static Row makeRow(String[] data) {
        List<Cell> cells = new ArrayList<>();

        for (String cell : data)
            cells.add(new Cell(cell));

        return new Row(cells);
    }


    public static List<Row> makeRows(String[][] data) {
        List<Row> rows = new ArrayList<>();

        for (String[] row : data)
            rows.add(makeRow(row));

        return rows;
    }

    public static Table makeTable(String name, Row header, List<Row> body, Row footer) {
        return new Table("royalties-table",
                         Arrays.asList(new Row[]{header}),
                         body,
                         Arrays.asList(new Row[]{footer}),
                         true);
    }

    public static Table makeTable(String name, Row header, List<Row> body, List<Row> footer) {
        return new Table("royalties-table",
                         Arrays.asList(new Row[]{header}),
                         body,
                         footer,
                         true);
    }
}
