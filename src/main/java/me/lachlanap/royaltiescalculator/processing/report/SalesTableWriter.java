package me.lachlanap.royaltiescalculator.processing.report;

import me.lachlanap.royaltiescalculator.processing.matcher.PayeeMatcherResults;
import org.apache.commons.lang3.StringEscapeUtils;

/**
 *
 * @author lachlan
 */
public class SalesTableWriter implements TableWriter {

    private boolean includeGST = false;
    private double subTotal;
    private double total;
    private boolean hasBeenFullyComputed;

    @Override
    public void includeGST() {
        includeGST = true;
    }

    @Override
    public String[] getHeader() {
        return new String[]{
            "Title", "Qty Sold", "Per Item Royalty", "% of Sell", "Total Selling", "Royalty"
        };
    }

    @Override
    public String[] makeRow(PayeeMatcherResults.Item item) {
        if (hasBeenFullyComputed)
            throw new IllegalStateException("Cannot add more data once table has been completed");

        String trimmedName = item.getItem().getName();
        if (trimmedName.length() > 50) {
            trimmedName = trimmedName.substring(0, 50);
            trimmedName = trimmedName.substring(0, trimmedName.lastIndexOf(" "));
        }

        double sales = item.getItem().getSales();
        int qty = item.getItem().getQtyOrdered();
        double unitSales = sales / qty;
        double unitRoyaltySales = unitSales * item.getPercentRoyalty() / 100.0;
        double royaltySales = sales * item.getPercentRoyalty() / 100.0;

        subTotal += royaltySales;

        return new String[]{
            StringEscapeUtils.escapeXml(trimmedName),
            "" + qty,
            Helpers.formatCurrency(unitRoyaltySales),
            item.getPercentRoyalty() + "%",
            Helpers.formatCurrency(sales),
            Helpers.formatCurrency(royaltySales)
        };
    }

    @Override
    public String[][] makeFooter() {
        hasBeenFullyComputed = true;
        total = subTotal;

        if (includeGST) {
            double gst = subTotal * 0.1d;
            total = total + gst;

            return new String[][]{
                {"Subtotal", "", "", "", "", Helpers.formatCurrency(subTotal)},
                {"GST", "", "", "", "", Helpers.formatCurrency(gst)},
                {"Total", "", "", "", "", Helpers.formatCurrency(total)}
            };
        } else
            return new String[][]{
                {"Total", "", "", "", "", Helpers.formatCurrency(total)}
            };
    }

    @Override
    public double getTotal() {
        if (!hasBeenFullyComputed)
            throw new IllegalStateException("Cannot access total until table has been completed");
        return total;
    }
}
