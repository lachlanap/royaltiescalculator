package me.lachlanap.royaltiescalculator.processing.report;

import me.lachlanap.royaltiescalculator.processing.matcher.PayeeMatcherResults;
import org.apache.commons.lang3.StringEscapeUtils;

/**
 *
 * @author lachlan
 */
public class RRPTableWriter implements TableWriter {

    private boolean includeGST = false;
    private double subTotal;
    private double total;
    private boolean hasBeenFullyComputed;

    @Override
    public void includeGST() {
        includeGST = true;
    }

    @Override
    public String[] getHeader() {
        return new String[]{
            "Title", "Qty Sold", "Per Item Royalty", "% of RRP", "RRP", "Royalty"
        };
    }

    @Override
    public String[] makeRow(PayeeMatcherResults.Item item) {
        if (hasBeenFullyComputed)
            throw new IllegalStateException("Cannot add more data once table has been completed");

        String trimmedName = item.getItem().getName();
        if (trimmedName.length() > 50) {
            trimmedName = trimmedName.substring(0, 50);
            trimmedName = trimmedName.substring(0, trimmedName.lastIndexOf(" "));
        }

        int qty = item.getItem().getQtyOrdered();
        double unitRRP = item.getRRP();
        double unitRoyalty = unitRRP * item.getPercentRoyalty() / 100.0;
        double royaltySales = unitRoyalty * qty;

        subTotal += royaltySales;

        return new String[]{
            StringEscapeUtils.escapeXml(trimmedName),
            "" + qty,
            Helpers.formatCurrency(unitRoyalty),
            item.getPercentRoyalty() + "%",
            Helpers.formatCurrency(unitRRP),
            Helpers.formatCurrency(royaltySales)
        };
    }

    @Override
    public String[][] makeFooter() {
        hasBeenFullyComputed = true;
        total = subTotal;

        if (includeGST) {
            double gst = subTotal / 11;

            return new String[][]{
                {"Includes GST", "", "", "", "", Helpers.formatCurrency(gst)},
                {"Total", "", "", "", "", Helpers.formatCurrency(total)}
            };
        } else
            return new String[][]{
                {"Total", "", "", "", "", Helpers.formatCurrency(total)}
            };
    }

    @Override
    public double getTotal() {
        if (!hasBeenFullyComputed)
            throw new IllegalStateException("Cannot access total until table has been completed");
        return total;
    }

    private String esc(String str) {
        return StringEscapeUtils.escapeXml(str);
    }
}
