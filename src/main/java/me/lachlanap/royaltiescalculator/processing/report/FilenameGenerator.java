/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.lachlanap.royaltiescalculator.processing.report;

import me.lachlanap.royaltiescalculator.data.Period;

/**
 *
 * @author lachlan
 */
public class FilenameGenerator {

    public String generateNameFor(Period period, String payeeIdentifier) {
        payeeIdentifier = payeeIdentifier
                .replaceAll(" ", "-")
                .toLowerCase();
        return replaceDuplicateDashes(String.format("%s-%s-royalties-%s.pdf",
                                                    period.getYear(),
                                                    period.getQuarter()
                                                    .toString(),
                                                    payeeIdentifier));
    }

    private String replaceDuplicateDashes(String in) {
        String fixed = in.replaceAll("--", "-");
        if (fixed.equals(in))
            return in;
        else
            return replaceDuplicateDashes(fixed);
    }
}
