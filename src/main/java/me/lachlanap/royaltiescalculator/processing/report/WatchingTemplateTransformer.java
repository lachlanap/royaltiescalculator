/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.lachlanap.royaltiescalculator.processing.report;

import java.io.IOException;
import org.docbag.Context;
import org.docbag.stream.MemoryInputStream;
import org.docbag.template.DocumentTemplateStream;
import org.docbag.template.MemoryTemplateStream;
import org.docbag.template.transformer.TemplateTransformer;
import org.docbag.template.transformer.content.xml.ContentHandlerFactory;
import org.docbag.template.transformer.content.xml.TemplateContentHandler;
import org.docbag.template.transformer.content.xml.XMLDynamicContentHandler;
import org.docbag.template.transformer.xslt.DefaultXSLTTemplateTransformer;

/**
 *
 * @author Lachlan Phillips
 */
class WatchingTemplateTransformer implements TemplateTransformer<DocumentTemplateStream> {

    private final DefaultXSLTTemplateTransformer dxtt;

    public WatchingTemplateTransformer() {
        dxtt = new DefaultXSLTTemplateTransformer(new ContentHandlerFactory<String>() {

            @Override
            public TemplateContentHandler<String> getContentHandler() {
                throw new UnsupportedOperationException(".getContentHandler not supported yet.");
            }

            @Override
            public TemplateContentHandler<String> getContentHandler(Context context) {
                return new XMLDynamicContentHandler(context);
            }
        }
        );
    }

    @Override
    public DocumentTemplateStream transform(DocumentTemplateStream template) {
        return dxtt.transform(template);
    }

    @Override
    public DocumentTemplateStream transform(DocumentTemplateStream template, Context context) {
        MemoryInputStream mis = new MemoryInputStream((MemoryInputStream) dxtt.transform(template, context).getStream());
        mis.mark(0);

        try {
            byte[] buf = new byte[1024];
            int length;
            while ((length = mis.read(buf)) >= 0) {
                System.out.print(new String(buf, 0, length));
            }
            System.out.println();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        mis.reset();
        return new MemoryTemplateStream(mis, "doc");
    }

}
