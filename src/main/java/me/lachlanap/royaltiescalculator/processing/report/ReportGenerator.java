package me.lachlanap.royaltiescalculator.processing.report;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.nio.file.*;
import java.util.*;
import me.lachlanap.royaltiescalculator.data.Payee;
import me.lachlanap.royaltiescalculator.data.Period;
import me.lachlanap.royaltiescalculator.data.Report;
import me.lachlanap.royaltiescalculator.processing.matcher.PayeeMatcherResults;
import org.apache.commons.lang3.StringEscapeUtils;
import org.docbag.*;
import org.docbag.table.Row;
import org.docbag.template.DocumentTemplateStream;
import org.docbag.template.repo.CachingDocumentTemplateRepository;
import org.docbag.template.repo.FileDocumentTemplateRepository;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author lachlan
 */
class ReportGenerator {

    private static final String TEMPLATE_NEWLINE_HACK = "<fo:block role=\"html:br\" />";

    private static final DateTimeFormatter fileDateFormatter = DateTimeFormat
            .forPattern("yyyy-MM-dd");
    private static final DateTimeFormatter readableDateFormatter = DateTimeFormat
            .forPattern("dd MMMM yyyy");

    private static final CachingDocumentTemplateRepository cachingRepo;

    static {
        cachingRepo = new CachingDocumentTemplateRepository();
        cachingRepo.registerRepository(new FileDocumentTemplateRepository("./"));
    }

    private final PayeeMatcherResults.PayeeResult result;
    private final Period period;

    private final FilenameGenerator filenameGenerator;

    private final LocalDate periodStart;
    private final LocalDate periodEnd;
    private final LocalDate today;

    private final EnumMap<Payee.PaymentMode, TableWriter> tableWriters;

    public ReportGenerator(PayeeMatcherResults.PayeeResult result, Period period) {
        this.result = result;
        this.period = period;

        filenameGenerator = new FilenameGenerator();

        this.periodStart = period.getStart();
        this.periodEnd = period.getEnd();
        this.today = new LocalDate();

        tableWriters = new EnumMap<>(Payee.PaymentMode.class);
        tableWriters.put(Payee.PaymentMode.Sales, new SalesTableWriter());
        tableWriters.put(Payee.PaymentMode.RRP, new RRPTableWriter());
    }

    public Report generateReport() throws IOException {
        Payee payee = result.getPayee();

        DocumentCreator<DocumentStream, DocumentTemplateStream> creator
                = DocBag.newDocumentCreator(cachingRepo);

        Path reportFilePath = makePDFReport(creator, "templates/basic.html");
        Report report = new Report(payee, period, reportFilePath);
        return report;
    }

    private Path makePDFReport(DocumentCreator<DocumentStream, DocumentTemplateStream> creator,
                               String templateName) throws IOException {
        Context<String, Object> context = new DefaultContext();

        context.put("date", readableDateFormatter.print(today));

        context.put("payee-name", esc(result.getPayee().getName()));
        context.put("payee-firstname", esc(result.getPayee().getFirstname()));

        context.put("payee-address", multilineHtml(result.getPayee()
                    .getAddress()));

        context.put("payee-company", esc(result.getPayee().getCompany()));

        context.put("period-start", readableDateFormatter.print(periodStart));
        context.put("period-end", readableDateFormatter.print(periodEnd));

        TableWriter tableWriter = tableWriters.get(result.getPayee()
                .getPaymentMode());
        if (result.getPayee().isGSTNeeded())
            tableWriter.includeGST();

        List<Row> tableRows = new ArrayList<>();
        for (PayeeMatcherResults.Item item : result.getItems()) {
            String[] rowData = tableWriter.makeRow(item);
            tableRows.add(Helpers.makeRow(rowData));
        }

        context.put("royalties-table", Helpers.makeTable("royalties-table",
                                                         Helpers
                                                         .makeRow(tableWriter
                                                                 .getHeader()),
                                                         tableRows,
                                                         Helpers
                                                         .makeRows(tableWriter
                                                                 .makeFooter())));
        context.put("total-value", Helpers
                    .formatCurrency(tableWriter.getTotal()));

        DocumentStream document = creator.createDocument(templateName, context);

        return writeOutPDF(document);
    }

    private String multilineHtml(String text) {
        return esc(text).replaceAll("\n", TEMPLATE_NEWLINE_HACK);
    }

    private Path writeOutPDF(DocumentStream document) throws IOException {
        Path path = makePath();
        if (!Files.exists(path.getParent(), LinkOption.NOFOLLOW_LINKS)) {
            Files.createDirectories(path.getParent());
        }

        try (BufferedInputStream bis = new BufferedInputStream(document
                .getStream());
             BufferedOutputStream bos = new BufferedOutputStream(Files
                     .newOutputStream(path,
                                      StandardOpenOption.CREATE,
                                      StandardOpenOption.TRUNCATE_EXISTING))) {
            byte[] buf = new byte[1024];

            int len;
            while ((len = bis.read(buf, 0, 1024)) != -1) {
                bos.write(buf, 0, len);
            }
        }

        return path;
    }

    private Path makePath() {
        // 2014-Q1 royalties chris-paton
        String payeeName = result.getPayee()
                .getNameOrCompany()
                .replaceAll(" ", "-")
                .toLowerCase();
        String filename = filenameGenerator.generateNameFor(period, result
                                                            .getPayee()
                                                            .getNameOrCompany());
        return Paths.get("reports", payeeName, filename);
    }

    private String esc(String str) {
        return StringEscapeUtils.escapeXml(str);
    }
}
