package me.lachlanap.royaltiescalculator.processing.report;

import me.lachlanap.royaltiescalculator.processing.matcher.PayeeMatcherResults;

/**
 * A class to separate the task of generating the table data from the table implementation.
 *
 * TableWriters are used for only one report.
 */
public interface TableWriter {

    public void includeGST();

    public String[] getHeader();

    public String[] makeRow(PayeeMatcherResults.Item item);

    public String[][] makeFooter();

    public double getTotal();
}
