package me.lachlanap.royaltiescalculator.processing.report;

import java.util.*;
import java.util.concurrent.*;
import me.lachlanap.royaltiescalculator.AsyncServices;
import me.lachlanap.royaltiescalculator.data.*;
import me.lachlanap.royaltiescalculator.database.Database;
import me.lachlanap.royaltiescalculator.processing.matcher.PayeeMatcherResults;

/**
 *
 * @author lachlan
 */
public class ReportsGenerator {

    private final PayeeMatcherResults matcherResults;
    private final Database database;
    private final Period period;
    private Runnable updateRunnable;
    private int total, done;

    public ReportsGenerator(PayeeMatcherResults matcherResults, Database database, Period period) {
        this.matcherResults = matcherResults;
        this.database = database;
        this.period = period;
    }

    public void setUpdateRunnable(Runnable updateRunnable) {
        this.updateRunnable = updateRunnable;
    }

    public int getDone() {
        return done;
    }

    public int getTotal() {
        return total;
    }

    public List<GeneratorResult> generateReports() {
        if (period == null)
            throw new IllegalStateException("Period must be defined");

        Map<Payee, Future<Report>> futures = new HashMap<>();

        for (final PayeeMatcherResults.PayeeResult payeeResult : matcherResults.getPayeeResults()) {
            futures.put(payeeResult.getPayee(), AsyncServices.getInstance().execute(new Callable<Report>() {

                @Override
                public Report call() throws Exception {
                    ReportGenerator rpg = new ReportGenerator(payeeResult, period);
                    Report report = rpg.generateReport();
                    database.saveReport(report);
                    return report;
                }
            }));
            total++;
        }

        boolean going = true;
        do {
            done = 0;
            for (Future<Report> future : futures.values()) {
                if (future.isDone())
                    done++;
            }

            if (done == total)
                going = false;

            updateRunnable.run();
        } while (going);

        List<GeneratorResult> results = new ArrayList<>();
        for (Map.Entry<Payee, Future<Report>> future : futures.entrySet()) {
            GeneratorResult result;
            try {
                Report r = future.getValue().get();
                result = GeneratorResult.success(r);
            } catch (InterruptedException ex) {
                throw new RuntimeException("Interrupted while retrieving reports", ex);
            } catch (ExecutionException ex) {
                if (ex.getCause() instanceof Exception)
                    result = GeneratorResult.failure(future.getKey(), (Exception) ex.getCause());
                else
                    result = GeneratorResult.failure(future.getKey(), ex);
            }

            results.add(result);
        }

        return results;
    }

    public static class GeneratorResult {

        public enum Status {

            Success, Failure
        }

        public final Status status;
        public final Report report;
        public final Payee payee;
        public final Exception exception;

        public GeneratorResult(Status status, Report report, Payee payee, Exception exception) {
            this.status = status;
            this.report = report;
            this.payee = payee;
            this.exception = exception;
        }

        public static GeneratorResult success(Report report) {
            return new GeneratorResult(Status.Success, report, null, null);
        }

        public static GeneratorResult failure(Payee payee, Exception exception) {
            return new GeneratorResult(Status.Failure, null, payee, exception);
        }
    }
}
