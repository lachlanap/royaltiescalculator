package me.lachlanap.royaltiescalculator.processing.parser;

import com.googlecode.jcsv.reader.CSVEntryParser;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.regex.Pattern;
import me.lachlanap.royaltiescalculator.processing.SalesData;

/**
 *
 * @author lachlan
 */
public class PastelCSVParser extends CSVParser {

    @Override
    public boolean fileLooksLike(Path file) throws IOException {
        if (!Files.exists(file, LinkOption.NOFOLLOW_LINKS))
            throw new FileNotFoundException("Sales data file not found: " + file);

        try (BufferedReader br = Files.newBufferedReader(file, StandardCharsets.UTF_8)) {
            String line;
            while ((line = br.readLine()) != null)
                if (line.startsWith("Item : "))
                    return true;
        }

        return false;
    }

    @Override
    public SalesData parse(Path file) throws IOException {
        if (!Files.exists(file, LinkOption.NOFOLLOW_LINKS))
            throw new FileNotFoundException("Sales data file not found: " + file);

        SalesData salesData = new SalesData();

        for (SalesData.Item item : loadItems(file)) {
            if (item.getName().equals(""))
                continue;
            salesData = salesData.addItem(item);
        }

        return salesData;
    }

    @Override
    protected CSVEntryParser<Optional<SalesData.Item>> makeItemParser() {
        return new ItemParser();
    }

    private static class ItemParser implements CSVEntryParser<Optional<SalesData.Item>> {

        private static final String ROW_IDENTIFIER = "Item : ";
        private final NumberFormat currencyReader;

        private ItemParser() {
            currencyReader = NumberFormat.getNumberInstance();
        }

        @Override
        public Optional<SalesData.Item> parseEntry(String... data) {
            // Skip rows not beginning with "Item : "
            if (!data[0].startsWith(ROW_IDENTIFIER))
                return Optional.none();

            String[] skuAndName = data[0]
                    .substring(ROW_IDENTIFIER.length())
                    .split(Pattern.quote(" - "));


            String sku = skuAndName[0].trim();
            String name = skuAndName[1].trim();
            int qty = Integer.parseInt(data[1].trim());
            double totalValue;

            try {
                totalValue = currencyReader.parse(data[2].trim()).doubleValue();
            } catch (ParseException pe) {
                throw new NumberFormatException("Failed to parse currency value: " + pe.getMessage());
            }

            SalesData.Item item = new SalesData.Item(sku, name, qty, totalValue);
            return Optional.some(item);
        }

    }
}
