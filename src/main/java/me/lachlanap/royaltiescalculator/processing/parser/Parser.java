package me.lachlanap.royaltiescalculator.processing.parser;

import java.io.IOException;
import java.nio.file.Path;
import me.lachlanap.royaltiescalculator.processing.SalesData;

/**
 *
 * @author lachlan
 */
public interface Parser {

    /**
     * A quick test to check whether the passed file 'looks like' it could be this format.
     * This is not meant to be an exhaustive check, just for the UI.
     */
    public boolean fileLooksLike(Path file) throws IOException;

    /**
     * Parse the passed file into SalesData.
     */
    public SalesData parse(Path file) throws IOException;

}
