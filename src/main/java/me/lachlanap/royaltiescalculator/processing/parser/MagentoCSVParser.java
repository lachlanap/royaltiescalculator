package me.lachlanap.royaltiescalculator.processing.parser;

import com.googlecode.jcsv.reader.CSVEntryParser;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Locale;
import me.lachlanap.royaltiescalculator.processing.SalesData;

/**
 *
 * @author lachlan
 */
public class MagentoCSVParser extends CSVParser {

    private static final String[] EXPECTED_HEADER = {"Period", "SKU", "Product Name", "Quantity Ordered", "Total Value"};
    /*private static final String EXPECTED_HEADER
     = "\"Period\",\"SKU\",\"Product Name\",\"Quantity Ordered\",\"Total Value\"";*/

    @Override
    public boolean fileLooksLike(Path file) throws IOException {
        if (!Files.exists(file, LinkOption.NOFOLLOW_LINKS))
            throw new FileNotFoundException("Sales data file not found: " + file);

        try (BufferedReader br = Files
                .newBufferedReader(file, StandardCharsets.UTF_8)) {
            String line = br.readLine().trim();
            if (line == null || line.isEmpty())
                return false;

            String[] header = line.split(",");
            if (header.length != EXPECTED_HEADER.length)
                return false;
            for (int i = 0; i < EXPECTED_HEADER.length; i++) {
                String test = header[i];
                if (test.startsWith("\""))
                    test = test.substring(1);
                if (test.endsWith("\""))
                    test = test.substring(0, test.length() - 1);

                if (!test.equals(EXPECTED_HEADER[i]))
                    return false;
            }

            return true;
        }
    }

    @Override
    public SalesData parse(Path file) throws IOException {
        if (!Files.exists(file, LinkOption.NOFOLLOW_LINKS))
            throw new FileNotFoundException("Sales data file not found: " + file);

        SalesData salesData = new SalesData();

        for (SalesData.Item item : loadItems(file)) {
            if (item == null)
                continue;
            salesData = salesData.addItem(item);
        }

        return salesData;
    }

    @Override
    protected CSVEntryParser<Optional<SalesData.Item>> makeItemParser() {
        return new ItemParser();
    }

    private static class ItemParser implements CSVEntryParser<Optional<SalesData.Item>> {

        private static final String[] TITLE = {"Period", "SKU", "Product Name", "Quantity Ordered", "Total Value"};
        private final NumberFormat currencyReader;

        private ItemParser() {
            currencyReader = NumberFormat.getCurrencyInstance(Locale.US);
        }

        @Override
        public Optional<SalesData.Item> parseEntry(String... data) {
            if (Arrays.equals(data, TITLE))
                return Optional.none();
            if (data[0].equals("Total")) // Skip the final row, which is totals
                return Optional.none();

            String sku = data[1].trim();
            String name = data[2].trim();
            int qty = Integer.parseInt(data[3].trim());
            double totalValue;

            try {
                totalValue = currencyReader.parse(data[4].trim()).doubleValue();
            } catch (ParseException pe) {
                throw new NumberFormatException("Failed to parse currency value: " + pe
                        .getMessage());
            }

            SalesData.Item item = new SalesData.Item(sku, name, qty, totalValue);
            return Optional.some(item);
        }

    }
}
