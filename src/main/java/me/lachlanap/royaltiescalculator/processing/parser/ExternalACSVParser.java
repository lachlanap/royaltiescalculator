package me.lachlanap.royaltiescalculator.processing.parser;

import com.googlecode.jcsv.reader.CSVEntryParser;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
import me.lachlanap.royaltiescalculator.processing.SalesData;

/**
 *
 * @author lachlan
 */
public class ExternalACSVParser extends CSVParser {

    private static final String[] EXPECTED_HEADER_BEGINNING = {"UTP", "Title", "Author", "Qty"};

    @Override
    public boolean fileLooksLike(Path file) throws IOException {
        if (!Files.exists(file, LinkOption.NOFOLLOW_LINKS))
            throw new FileNotFoundException("Sales data file not found: " + file);

        try (BufferedReader br = Files
                .newBufferedReader(file, StandardCharsets.UTF_8)) {
            String line = br.readLine().trim();
            if (line == null || line.isEmpty())
                return false;

            String[] header = line.split(",");
            if (header.length < EXPECTED_HEADER_BEGINNING.length)
                return false;
            for (int i = 0; i < EXPECTED_HEADER_BEGINNING.length; i++) {
                String test = header[i];
                if (test.startsWith("\""))
                    test = test.substring(1);
                if (test.endsWith("\""))
                    test = test.substring(0, test.length() - 1);

                if (!test.equals(EXPECTED_HEADER_BEGINNING[i]))
                    return false;
            }

            return true;
        }
    }

    @Override
    public SalesData parse(Path file) throws IOException {
        if (!Files.exists(file, LinkOption.NOFOLLOW_LINKS))
            throw new FileNotFoundException("Sales data file not found: " + file);

        SalesData salesData = new SalesData();

        for (SalesData.Item item : loadItems(file)) {
            if (item == null)
                continue;
            salesData = salesData.addItem(item);
        }

        return salesData;
    }

    @Override
    protected CSVEntryParser<Optional<SalesData.Item>> makeItemParser() {
        return new ItemParser();
    }

    private static class ItemParser implements CSVEntryParser<Optional<SalesData.Item>> {

        private final NumberFormat currencyReader;

        private ItemParser() {
            currencyReader = NumberFormat.getCurrencyInstance(Locale.US);
        }

        @Override
        public Optional<SalesData.Item> parseEntry(String... data) {
            if (startsWith(data, EXPECTED_HEADER_BEGINNING))
                return Optional.none();
            if (data[0].equals("Total")) // Skip the final row, which is totals
                return Optional.none();

            String sku = data[0].trim() + 'r';
            String name = data[1].trim();
            String qtyStr = data[3].trim();
            String royaltyStr = data[4].trim();

            if (sku.isEmpty() || name.isEmpty() || qtyStr.isEmpty() || royaltyStr.isEmpty())
                return Optional.none();

            int qty = Integer.parseInt(qtyStr);
            double totalValue;

            try {
                totalValue = currencyReader.parse(royaltyStr).doubleValue() / (20 / 100.0);
            } catch (ParseException pe) {
                throw new NumberFormatException("Failed to parse currency value: " + pe
                        .getMessage());
            }

            SalesData.Item item = new SalesData.Item(sku, name, qty, totalValue);
            return Optional.some(item);
        }

        private boolean startsWith(String[] test, String[] expected) {
            for (int i = 0; i < expected.length; i++) {
                if (i >= test.length)
                    return false;
                if (!test[i].equals(expected[i]))
                    return false;
            }

            return true;
        }
    }
}
