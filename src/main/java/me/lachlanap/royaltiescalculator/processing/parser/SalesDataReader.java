package me.lachlanap.royaltiescalculator.processing.parser;

import java.io.IOException;
import java.nio.file.Path;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import me.lachlanap.royaltiescalculator.processing.SalesData;

/**
 *
 * @author lachlan
 */
public class SalesDataReader {

    private final Map<DataFile.Format, Parser> parsers;

    public SalesDataReader() {
        parsers = new EnumMap<>(DataFile.Format.class);
        parsers.put(DataFile.Format.MagentoCSV, new MagentoCSVParser());
        parsers.put(DataFile.Format.PastelCSV, new PastelCSVParser());
        parsers.put(DataFile.Format.ExternalACSV, new ExternalACSVParser());
    }

    /**
     * Guesses the format of the specified file.
     *
     * @throws IllegalArgumentException if it cannot guess the format
     */
    public DataFile.Format guessFormatOf(Path path) throws IOException {
        for (Map.Entry<DataFile.Format, Parser> parser : parsers.entrySet()) {
            if (parser.getValue().fileLooksLike(path))
                return parser.getKey();
        }

        throw new IllegalArgumentException("Could not guess format of " + path);
    }

    /**
     * Parses the list of files.
     *
     * @throws IllegalArgumentException if it cannot find the parser for a file.
     */
    public SalesData parse(List<DataFile> files) throws IOException {
        SalesData salesData = new SalesData();

        for (DataFile dataFile : files) {
            Parser parser = parsers.get(dataFile.getFormat());
            if (parser == null)
                throw new IllegalArgumentException("Unknown sales data format: " + dataFile.getFormat());

            SalesData parsed = parser.parse(dataFile.getPath());
            salesData = salesData.append(parsed);
        }
        System.out.println(salesData);

        return salesData;
    }

}
