package me.lachlanap.royaltiescalculator.processing.parser;

import com.googlecode.jcsv.CSVStrategy;
import com.googlecode.jcsv.reader.CSVEntryParser;
import com.googlecode.jcsv.reader.CSVReader;
import com.googlecode.jcsv.reader.internal.CSVReaderBuilder;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.MalformedInputException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import me.lachlanap.royaltiescalculator.processing.SalesData;

/**
 *
 * @author lachlan
 */
public abstract class CSVParser implements Parser {

    @Override
    public abstract boolean fileLooksLike(Path file) throws IOException;

    @Override
    public abstract SalesData parse(Path file) throws IOException;

    protected Iterable<SalesData.Item> loadItems(Path file) throws IOException {
        Reader reader = Files.newBufferedReader(file, StandardCharsets.UTF_8);
        CSVReader<Optional<SalesData.Item>> csvReader = new CSVReaderBuilder<Optional<SalesData.Item>>(reader)
                .entryParser(makeItemParser())
                .strategy(CSVStrategy.UK_DEFAULT).build();

        csvReader.readHeader();

        List<SalesData.Item> items = new ArrayList<>();

        Optional<SalesData.Item> next;
        SalesData.Item item = null;
        try {
            while ((next = csvReader.readNext()) != null) {
                if (next.isPresent()) {
                    item = next.get();
                    items.add(item);
                }
            }
        } catch (MalformedInputException mie) {
            throw new IllegalArgumentException(
                    file.getFileName() + " is not encoded in UTF-8 (after "
                    + (item == null ? "the first line" : item.getSku()) + "). "
                    + "Some data may have not been parsed.", mie);
        }

        return items;
    }

    protected abstract CSVEntryParser<Optional<SalesData.Item>> makeItemParser();

}
