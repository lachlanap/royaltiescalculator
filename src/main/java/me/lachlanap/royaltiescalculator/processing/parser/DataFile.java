package me.lachlanap.royaltiescalculator.processing.parser;

import java.nio.file.Path;

/**
 *
 * @author lachlan
 */
public class DataFile {

    public enum Format {

        MagentoCSV("Magento CSV"), PastelCSV("Pastel CSV"), ExternalACSV("External A");

        private final String name;

        private Format(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }

    }

    private final Path path;
    private final Format format;

    public DataFile(Path path, Format format) {
        this.path = path;
        this.format = format;
    }

    public Path getPath() {
        return path;
    }

    public Format getFormat() {
        return format;
    }

    @Override
    public String toString() {
        return "[DataFile " + path + "; " + format + "]";
    }
}
