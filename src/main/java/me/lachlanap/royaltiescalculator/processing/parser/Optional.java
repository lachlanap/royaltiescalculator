package me.lachlanap.royaltiescalculator.processing.parser;

/**
 *
 * @author lachlan
 */
public class Optional<T> {

    private final boolean present;
    private final T value;

    public static <T> Optional<T> some(T value) {
        return new Optional<>(true, value);
    }

    public static <T> Optional<T> none() {
        return new Optional<>(false, null);
    }

    private Optional(boolean present, T value) {
        this.present = present;
        this.value = value;
    }

    public boolean isPresent() {
        return present;
    }

    public T get() {
        if (!present)
            throw new IllegalStateException("Cannot get the value of a non-present optional");
        else
            return value;
    }
}
