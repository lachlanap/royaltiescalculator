package me.lachlanap.royaltiescalculator;

import java.util.concurrent.*;

/**
 *
 * @author lachlan
 */
public class AsyncServices {

    private static final AsyncServices asyncServices = new AsyncServices();
    private final ExecutorService executorService;

    private AsyncServices() {
        executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        //executorService = Executors.newFixedThreadPool(2);
    }

    public <T> Future<T> execute(final Callable<T> callable) {
        return executorService.submit(new Callable<T>() {

            @Override
            public T call() throws Exception {
                try {
                    return callable.call();
                } catch (Exception e) {
                    e.printStackTrace();
                    throw e;
                }
            }
        });
    }

    public void execute(final Runnable runnable) {
        executorService.submit(new Runnable() {

            @Override
            public void run() {
                try {
                    runnable.run();
                } catch (RuntimeException e) {
                    e.printStackTrace();
                    throw e;
                }
            }
        });
    }

    public void shutdown() {
        executorService.shutdownNow();
    }

    public static <T> T unfuture(Future<T> future) {
        try {
            return future.get();
        } catch (ExecutionException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static AsyncServices getInstance() {
        return asyncServices;
    }
}
