package me.lachlanap.royaltiescalculator;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;
import me.lachlanap.royaltiescalculator.database.Database;
import me.lachlanap.royaltiescalculator.gui.MainGUI;
import me.lachlanap.royaltiescalculator.gui.Messaging;

/**
 *
 * @author lachlan
 */
public class Main {

    public static void main(String[] args) {
        Database database = chooseDatabase(args);

        trySetLaF();
        initialiseMainGUI(database);

        initialiseDB(database);

        if (database.isNewVersion())
            askToImportData(database);
    }

    private static Database chooseDatabase(String[] args) {
        Database db;
        if (args.length == 0)
            db = Database.newStandardDatabase();
        else {
            switch (args[0]) {
                case "-debug-basicmock":
                    db = Database.newMockDatabase(true);
                    break;
                case "-debug-intensivemock":
                    db = Database.newIntensiveMockDatabase();
                    break;
                case "-debug-massivemock":
                    db = Database.newStressMockDatabase();
                    break;
                default:
                    db = Database.newStandardDatabase();
                    break;
            }
        }
        return db;
    }

    private static void trySetLaF() {
        try {
            String gtkLaFClassName = null;
            for (LookAndFeelInfo laf : UIManager.getInstalledLookAndFeels()) {
                if (laf.getName().toLowerCase().contains("gtk"))
                    gtkLaFClassName = laf.getClassName();
            }

            if (gtkLaFClassName != null)
                UIManager.setLookAndFeel(gtkLaFClassName);
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException ex1) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex1);

            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException ex2) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex2);
            }
        }
    }

    private static void initialiseMainGUI(final Database database) {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                MainGUI mainGUI = new MainGUI(database);
                mainGUI.addWindowListener(new WindowAdapter() {

                    @Override
                    public void windowClosing(WindowEvent e) {
                        closeDatabase(database);
                    }

                });
                mainGUI.setVisible(true);
            }
        });
    }

    private static void closeDatabase(Database database) {
        try {
            database.autoExport();
        } catch (IOException ioe) {
            Messaging.exception(ioe, "Error auto-exporting database", null);
        }

        try {
            database.dispose();
        } catch (IOException ioe) {
            Messaging.exception(ioe, "Error closing database", null);
        }
    }

    private static void initialiseDB(Database database) {
        try {
            database.init();
        } catch (final IOException ioe) {
            Messaging.exception(ioe, "Error connecting to database", null);
            System.exit(1);
        }
    }

    private static void askToImportData(Database database) {
        try {
            if (Messaging.askYesNo("Database was updated. "
                                   + "Do you want to import old data from last auto-export?")) {
                try {
                    database.importFromLastAutoExport();
                } catch (IOException ioe) {
                    Messaging.exception(ioe, "Failed to import from last auto-export", null);
                }
            }
        } catch (InterruptedException | InvocationTargetException ex) {
            Messaging.exception(ex, "Failed to get result of question", null);
        }
    }
}
