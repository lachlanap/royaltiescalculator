/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.lachlanap.royaltiescalculator.export;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import me.lachlanap.royaltiescalculator.ApplicationInfo;
import me.lachlanap.royaltiescalculator.AsyncServices;
import me.lachlanap.royaltiescalculator.data.MatcherRule;
import me.lachlanap.royaltiescalculator.data.Payee;
import me.lachlanap.royaltiescalculator.data.Payee.PaymentMode;
import me.lachlanap.royaltiescalculator.data.Report;
import me.lachlanap.royaltiescalculator.database.Database;
import org.joda.time.LocalDate;

/**
 *
 * @author Lachlan Phillips
 */
public class Exporter {

    public void exportData(Database database, Path out) throws IOException {
        JsonObject root = new JsonObject();

        JsonArray payees = new JsonArray();
        for (Payee payee : AsyncServices.unfuture(database.getPayees()))
            payees.add(payeeToJson(payee));
        root.add("payees", payees);

        JsonArray rules = new JsonArray();
        for (MatcherRule rule : AsyncServices.unfuture(database.getMatcherRules()))
            rules.add(ruleToJson(rule));
        root.add("rules", rules);

        JsonArray reports = new JsonArray();
        for (Report report : AsyncServices.unfuture(database.getReports()))
            if (reportIsValid(report))
                reports.add(reportToJson(report));
        root.add("reports", reports);

        Gson gson = new Gson();
        try (JsonWriter writer = new JsonWriter(Files.newBufferedWriter(out, StandardCharsets.UTF_8))) {
            gson.toJson(root, writer);
        }
    }

    private boolean reportIsValid(Report report) {
        return !report.getPeriod().getStart().isAfter(new LocalDate());
    }

    private JsonElement payeeToJson(Payee payee) {
        JsonObject obj = new JsonObject();
        obj.addProperty("id", payee.getID().asInt());
        obj.addProperty("name", payee.getName());
        obj.addProperty("email", payee.getEmailAddress());
        obj.addProperty("company", payee.getCompany());
        obj.addProperty("payment-mode", payee.getPaymentMode().toString().toLowerCase());

        if (!payee.getAddress().isEmpty())
            obj.addProperty("street-address", payee.getAddress());
        if (payee.isGSTNeeded())
            obj.addProperty("gst-needed", true);
        return obj;
    }

    private JsonElement ruleToJson(MatcherRule rule) {
        JsonObject obj = new JsonObject();
        obj.addProperty("payee", rule.getPayee().getName());
        obj.addProperty("payee-id", rule.getPayee().getID().asInt());
        obj.addProperty("pattern", rule.getPattern());
        obj.addProperty("percent-royalty", rule.getPercentRoyalty());

        if (rule.getPayee().getPaymentMode() == PaymentMode.RRP)
            obj.addProperty("rrp", rule.getRRP());
        return obj;
    }

    private JsonElement reportToJson(Report report) {
        JsonObject obj = new JsonObject();
        obj.addProperty("payee", report.getPayee().getName());
        obj.addProperty("payee-id", report.getPayee().getID().asInt());
        obj.addProperty("period", periodForReport(report));
        obj.addProperty("report-path", reportPathToString(report));
        return obj;
    }

    private String reportPathToString(Report report) {
        Path reportPath = report.getReportFilePath();

        int i = 0;
        for (Path piece : reportPath) {
            if (piece.toString().equals("reports"))
                break;
            i++;
        }
        if (i < reportPath.getNameCount())
            reportPath = reportPath.subpath(i, reportPath.getNameCount());

        try {
            return ApplicationInfo.get().getWorkingDirectory()
                    .relativize(reportPath)
                    .normalize()
                    .toString();
        } catch (IllegalArgumentException iae) {
            return reportPath.normalize().toString();
        }
    }

    private String periodForReport(Report report) {
        return report.getPeriod().getYear() + "-" + report.getPeriod().getQuarter().name();
    }
}
