/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.lachlanap.royaltiescalculator.export;

import com.google.gson.*;
import com.google.gson.stream.JsonReader;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import me.lachlanap.royaltiescalculator.data.*;
import me.lachlanap.royaltiescalculator.data.Payee.PaymentMode;
import me.lachlanap.royaltiescalculator.database.Database;
import org.joda.time.LocalDate;

/**
 *
 * @author Lachlan Phillips
 */
public class Importer {

    public void importData(Database database, Path in) throws IOException {
        JsonObject obj;
        try (JsonReader reader = new JsonReader(Files.newBufferedReader(in, StandardCharsets.UTF_8))) {
            JsonParser parser = new JsonParser();
            obj = (JsonObject) parser.parse(reader);
        }

        database.wipe();

        JsonArray payeesJson = obj.getAsJsonArray("payees");
        Map<String, Payee> nameToPayee = new HashMap<>();
        Map<Integer, Payee> idToPayee = new HashMap<>();

        for (JsonElement e : payeesJson) {
            Payee payee = jsonToPayee((JsonObject) e, idToPayee);
            nameToPayee.put(payee.getName(), payee);

            database.savePayee(payee);
        }

        JsonArray rulesJson = obj.getAsJsonArray("rules");
        for (JsonElement e : rulesJson) {
            MatcherRule rule = jsonToRule((JsonObject) e, nameToPayee, idToPayee);
            database.saveRule(rule);
        }

        JsonArray reportsJson = obj.getAsJsonArray("reports");
        for (JsonElement e : reportsJson) {
            Report report = jsonToReport((JsonObject) e, nameToPayee, idToPayee);
            if (reportIsValid(report))
                database.saveReport(report);
        }
    }

    private boolean reportIsValid(Report report) {
        return !report.getPeriod().getStart()
                .isAfter(new LocalDate());
    }

    private Payee jsonToPayee(JsonObject json, Map<Integer, Payee> idToPayee) {
        int id = getJsonNumber(json.get("id"), -1).intValue();
        String name = getJsonString(json.get("name"), "ParsingError");
        String address = getJsonString(json.get("street-address"), "");
        String email = getJsonString(json.get("email"), "");
        String company = getJsonString(json.get("company"), "");

        PaymentMode paymentMode;
        String paymentModeString = getJsonString(json.get("payment-mode"), "sales").toLowerCase();
        switch (paymentModeString) {
            case "sales":
                paymentMode = PaymentMode.Sales;
                break;
            case "rrp":
                paymentMode = PaymentMode.RRP;
                break;
            default:
                paymentMode = PaymentMode.Sales;
                System.err.println("Invalid payment mode for " + name + ": " + paymentModeString);
        }

        boolean needsGst = getJsonBoolean(json.get("gst-needed"), false);

        Payee p = new Payee(name, address, email, company, paymentMode, needsGst);;

        if (id != -1)
            idToPayee.put(id, p);

        return p;
    }

    private MatcherRule jsonToRule(JsonObject json, Map<String, Payee> nameToPayee, Map<Integer, Payee> idToPayee) {
        String payeeName = json.get("payee").getAsString();
        int payeeID = getJsonNumber(json.get("payee-id"), -1).intValue();
        String pattern = json.get("pattern").getAsString();
        int percentRoyalty = getJsonNumber(json.get("percent-royalty"), 0).intValue();
        double rrp = getJsonNumber(json.get("rrp"), 0).doubleValue();

        if (payeeID >= 0 && idToPayee.containsKey(payeeID))
            return new MatcherRule(pattern,
                                   idToPayee.get(payeeID),
                                   percentRoyalty, rrp);
        else
            return new MatcherRule(pattern,
                                   nameToPayee.get(payeeName),
                                   percentRoyalty, rrp);
    }

    private Report jsonToReport(JsonObject json, Map<String, Payee> nameToPayee, Map<Integer, Payee> idToPayee) {
        String payeeName = json.get("payee").getAsString();
        int payeeID = getJsonNumber(json.get("payee-id"), -1).intValue();
        String periodString = json.get("period").getAsString();
        String reportPathString = getJsonString(json.get("report-path"), "");

        if (payeeID >= 0 && idToPayee.containsKey(payeeID))
            return new Report(idToPayee.get(payeeID),
                              stringToPeriod(periodString),
                              Paths.get(reportPathString));
        else
            return new Report(nameToPayee.get(payeeName),
                              stringToPeriod(periodString),
                              Paths.get(reportPathString));
    }

    private Period stringToPeriod(String period) {
        String[] yearQuarter = period.split("-");

        int year = Integer.parseInt(yearQuarter[0]);
        Quarter quarter = Quarter.valueOf(yearQuarter[1]);

        return new Period(year, quarter);
    }

    private String getJsonString(JsonElement value, String def) {
        if (value != null
            && value.isJsonPrimitive()
            && ((JsonPrimitive) value).isString())
            return value.getAsString();
        else
            return def;
    }

    private Number getJsonNumber(JsonElement value, Number def) {
        if (value != null
            && value.isJsonPrimitive()
            && ((JsonPrimitive) value).isNumber())
            return value.getAsNumber();
        else
            return def;
    }

    private boolean getJsonBoolean(JsonElement value, boolean def) {
        if (value != null
            && value.isJsonPrimitive()
            && ((JsonPrimitive) value).isBoolean())
            return value.getAsBoolean();
        else
            return def;
    }
}
