/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.lachlanap.royaltiescalculator.data;

/**
 *
 * @author Lachlan Phillips
 */
public enum Quarter {

    Q1, Q2, Q3, Q4;

    public Quarter previous() {
        switch (this) {
            case Q1:
                return Q4;
            case Q2:
                return Q1;
            case Q3:
                return Q2;
            case Q4:
                return Q3;
            default:
                throw new IllegalStateException("Should never happen");
        }
    }

    public int getStartMonth() {
        switch (this) {
            case Q1:
                return 1;
            case Q2:
                return 4;
            case Q3:
                return 7;
            case Q4:
                return 10;
            default:
                throw new IllegalStateException("Should never happen");
        }
    }

    public static Quarter fromMonth(int month) {
        if (month <= 3)
            return Q1;
        else if (month <= 6)
            return Q2;
        else if (month <= 9)
            return Q3;
        else
            return Q4;
    }
}
