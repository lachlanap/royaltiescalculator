package me.lachlanap.royaltiescalculator.data;

import java.util.regex.Pattern;
import me.lachlanap.royaltiescalculator.processing.SalesData;

/**
 *
 * @author lachlan
 */
public class MatcherRule {

    private final ID id;
    private final Pattern pattern;
    private final Payee payee;
    private final int percentRoyalty;

    private final double rrp;

    public MatcherRule(String pattern, Payee payee, int percentRoyalty, double rrp) {
        this(ID.nullID(), pattern, payee, percentRoyalty, rrp);
    }

    public MatcherRule(ID id, String pattern, Payee payee, int percentRoyalty, double rrp) {
        if (!pattern.startsWith("^"))
            pattern = "^" + pattern;
        this.id = id;
        this.pattern = Pattern.compile(pattern);
        this.payee = payee;
        this.percentRoyalty = percentRoyalty;
        this.rrp = rrp;
    }

    public boolean matches(SalesData.Item item) {
        return pattern.matcher(item.getSku()).find();
    }

    public ID getID() {
        return id;
    }

    public Payee getPayee() {
        return payee;
    }

    public int getPercentRoyalty() {
        return percentRoyalty;
    }

    public String getPattern() {
        return pattern.pattern();
    }

    public double getRRP() {
        return rrp;
    }

    @Override
    public String toString() {
        return payee + ": "
               + getPattern() + " "
               + percentRoyalty + "%"
               + ((rrp != 0) ? (" (" + rrp + " RRP)") : "");
    }

}
