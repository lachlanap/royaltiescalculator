package me.lachlanap.royaltiescalculator.data;

import java.util.Objects;

/**
 *
 * @author lachlan
 */
public class Payee {

    public static Payee fromName(String name) {
        return new Payee(ID.nullID(),
                         name,
                         "", "", "", PaymentMode.Sales, false);
    }

    public static Payee fromNameAddressEmail(String name, String address, String emailAddress) {
        return new Payee(ID.nullID(), name, address, emailAddress,
                         "", PaymentMode.Sales, false);
    }

    public enum PaymentMode {

        Sales, RRP
    }

    private final ID id;
    private final String name;
    private final String address;
    private final String emailAddress;
    private final String company;

    private final PaymentMode paymentMode;
    private final boolean gstNeeded;

    public Payee(String name, String address, String emailAddress, String company,
                 PaymentMode paymentMode, boolean gstNeeded) {
        this(ID.nullID(), name, address, emailAddress, company, paymentMode, gstNeeded);
    }

    public Payee(ID id,
                 String name, String address, String emailAddress, String company,
                 PaymentMode paymentMode, boolean gstNeeded) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.emailAddress = emailAddress;
        this.company = company;
        this.paymentMode = paymentMode;
        this.gstNeeded = gstNeeded;
    }

    public ID getID() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getCompany() {
        return company;
    }

    public PaymentMode getPaymentMode() {
        return paymentMode;
    }

    public boolean isGSTNeeded() {
        return gstNeeded;
    }

    public String getFirstname() {
        int firstSpace = name.indexOf(" ");
        if (firstSpace > 0)
            return name.substring(0, firstSpace);
        else
            return name;
    }

    public String getNameOrCompany() {
        if (name.isEmpty())
            return company;
        else
            return name;
    }

    @Override
    public String toString() {
        return getNameOrCompany();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Payee other = (Payee) obj;
        return Objects.equals(this.name, other.name);
    }


}
