package me.lachlanap.royaltiescalculator.data;

import java.nio.file.Path;
import java.util.Objects;

/**
 *
 * @author lachlan
 */
public class Report {

    private final ID id;
    private final Payee payee;
    private final Period period;
    private final Path reportFilePath;

    public Report(Payee payee, Period period, Path reportFilePath) {
        this(ID.nullID(), payee, period, reportFilePath);
    }

    public Report(ID id, Payee payee, Period period, Path reportFilePath) {
        this.id = id;
        this.payee = payee;
        this.period = period;
        this.reportFilePath = reportFilePath;
    }

    public ID getID() {
        return id;
    }

    public Payee getPayee() {
        return payee;
    }

    public Period getPeriod() {
        return period;
    }

    public Path getReportFilePath() {
        return reportFilePath;
    }

    @Override
    public String toString() {
        return "Report{payee=" + payee
               + ", period=" + period
               + ", reportFilePath=" + reportFilePath + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.payee);
        hash = 97 * hash + Objects.hashCode(this.period);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Report other = (Report) obj;
        if (!Objects.equals(this.payee, other.payee))
            return false;
        return Objects.equals(this.period, other.period);
    }
}
