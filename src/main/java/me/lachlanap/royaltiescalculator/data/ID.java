/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.lachlanap.royaltiescalculator.data;

/**
 *
 * @author Lachlan Phillips
 */
public class ID {

    private static final int NULL_INDEX = -1;

    public static ID fromInt(int id) {
        return new ID(id);
    }

    public static ID nullID() {
        return new ID(NULL_INDEX);
    }


    private int index;

    private ID(int index) {
        this.index = index;
    }

    public int asInt() {
        return index;
    }

    public boolean isNull() {
        return index == NULL_INDEX;
    }

    public void updateFromDB(int newID) {
        if (!isNull())
            throw new IllegalStateException("Cannot change an ID once it is set");
        else
            index = newID;
    }

    public void updateFromDB(ID copyID) {
        if (!isNull())
            throw new IllegalStateException("Cannot change an ID once it is set");
        else
            index = copyID.index;
    }

    @Override
    public String toString() {
        return "ID(index=" + index + ')';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (!(obj instanceof ID))
            return false;

        ID other = (ID) obj;
        return !isNull() && !other.isNull()
               && index == other.index;
    }
}
