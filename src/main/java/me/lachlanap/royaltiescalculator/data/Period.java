/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.lachlanap.royaltiescalculator.data;

import java.util.Objects;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author Lachlan Phillips
 */
public class Period {

    private static final DateTimeFormatter FORMATTER = DateTimeFormat.forPattern("dd MMM yyyy");

    public static Period getCurrentPeriod() {
        LocalDate now = LocalDate.now();
        return new Period(now.getYear(), Quarter.fromMonth(now.getMonthOfYear()));
    }

    public static Period getPreviousPeriod() {
        return getCurrentPeriod().previous();
    }

    public static Period forDate(LocalDate date) {
        return new Period(date.getYear(), Quarter.fromMonth(date.getMonthOfYear()));
    }

    private final int year;
    private final Quarter quarter;
    private final LocalDate start, end;

    public Period(int year, Quarter quarter) {
        this.year = year;
        this.quarter = quarter;

        this.start = new LocalDate(year, quarter.getStartMonth(), 1);
        this.end = start.plusMonths(3).minusDays(1);
    }

    public int getYear() {
        return year;
    }

    public Quarter getQuarter() {
        return quarter;
    }

    public LocalDate getStart() {
        return start;
    }

    public LocalDate getEnd() {
        return end;
    }

    public Period previous() {
        if (quarter == Quarter.Q1)
            return new Period(year - 1, quarter.previous());
        else
            return new Period(year, quarter.previous());
    }


    /**
     * Returns this period as a string formatted like: <code>01 Jan 2014 - 31 Mar 2014</code>.
     */
    public String toMonthRangeString() {
        return start.toString(FORMATTER) + " - " + end.toString(FORMATTER);
    }

    @Override
    public String toString() {
        return toMonthRangeString();
    }


    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + this.year;
        hash = 41 * hash + Objects.hashCode(this.quarter);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Period other = (Period) obj;
        if (this.year != other.year)
            return false;
        return this.quarter == other.quarter;
    }

}
