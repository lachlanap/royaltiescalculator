package me.lachlanap.royaltiescalculator.database;

import java.io.IOException;
import java.util.*;
import me.lachlanap.royaltiescalculator.data.*;

/**
 *
 * @author lachlan
 */
class MemoryDatabaseImpl implements DatabaseImpl {

    private final int testDataIncluded;

    private final List<Payee> payees;
    private final List<MatcherRule> rules;
    private final List<Report> reports;

    public MemoryDatabaseImpl(int testDataIncluded) {
        this.testDataIncluded = testDataIncluded;

        this.payees = new ArrayList<>();
        this.rules = new ArrayList<>();
        this.reports = new ArrayList<>();
    }

    @Override
    public void init() throws IOException {
        if (testDataIncluded == 0)
            return;

        payees.add(Payee.fromNameAddressEmail("Shauna Hicks",
                                              "8 Parkside Walk\n"
                                              + "Hoopers Crossing VIC 3029",
                                              ""));
        payees.add(Payee.fromNameAddressEmail("Graham Hendrick",
                                              "9 Parkside Walk\n"
                                              + "Somewhere Else VIC 3028",
                                              ""));
        payees.add(Payee.fromNameAddressEmail("Second Person",
                                              "2 Second Rd\n"
                                              + "Hoopers Crossing VIC 3029",
                                              "flipsidelachy@gmail.com"));

        rules.add(new MatcherRule(ID.nullID(), "^UTPE01", payees.get(0), 20, 0));
        rules.add(new MatcherRule(ID.nullID(), "^UTPE03", payees.get(1), 20, 0));
        rules.add(new MatcherRule(ID.nullID(), "^AUE5", payees.get(2), 20, 0));
        rules.add(new MatcherRule(ID.nullID(), "^AUE3", payees.get(2), 50, 0));
        rules.add(new MatcherRule(ID.nullID(), "^AUE7", payees.get(2), 15, 0));


        if (testDataIncluded == 2) {
            for (int i = 0; i < 200; i++) {
                Payee payee = Payee.fromName("P " + i);

                payees.add(payee);

                if (i % 50 == 0) {
                    for (int j = 0; j < 500; j++)
                        rules.add(new MatcherRule(ID.nullID(), "^AUE" + (i * 100) + "-" + (j * 1), payee, 10, 0));
                } else if (i % 5 == 0) {
                    for (int j = 0; j < 50; j++)
                        rules.add(new MatcherRule(ID.nullID(), "^AUE" + (i * 100) + "-" + (j * 23), payee, 10, 0));
                } else
                    rules.add(new MatcherRule(ID.nullID(), "^AUE" + (i), payee, 10, 0));
            }
        } else if (testDataIncluded == 3) {
            for (int i = 0; i < 1000; i++) {
                Payee payee = Payee.fromName("P " + i);

                payees.add(payee);

                for (int j = 0; j < 1000; j++)
                    rules.add(new MatcherRule(ID.nullID(), "^AUE"
                                                           + String.format("%04d", i)
                                                           + "-" + String.format("%04d", j),
                                              payee, 10, 0));
            }
        }
    }

    @Override
    public boolean isNewVersion() {
        return false;
    }

    @Override
    public void dispose() throws IOException {
        payees.clear();
        rules.clear();
        reports.clear();
    }

    @Override
    public List<MatcherRule> getMatcherRules() {
        return Collections.unmodifiableList(rules);
    }

    @Override
    public List<Payee> getPayees() {
        return Collections.unmodifiableList(payees);
    }

    @Override
    public List<Report> getReports(Period period) {
        Set<Report> theReports = new HashSet<>();
        for (Report r : reports) {
            if (r.getPeriod().equals(period)) {
                theReports.remove(r);

                theReports.add(r);
            }
        }

        return Collections.unmodifiableList(new ArrayList<>(theReports));
    }

    @Override
    public List<Report> getReports() {
        return Collections.unmodifiableList(reports);
    }

    @Override
    public void savePayee(Payee payee) throws IOException {
        if (payee.getID().isNull()) {
            payees.add(payee);
            payee.getID().updateFromDB(payees.size());
        } else {
            for (ListIterator<Payee> it = payees.listIterator(); it.hasNext();) {
                Payee test = it.next();
                if (test.getID() == payee.getID()) {
                    it.set(payee);
                }
            }
        }
    }

    @Override
    public void saveRule(MatcherRule rule) throws IOException {
        if (rule.getID().isNull()) {
            rules.add(rule);
            rule.getID().updateFromDB(rules.size());
        } else {
            for (ListIterator<MatcherRule> it = rules.listIterator(); it.hasNext();) {
                MatcherRule test = it.next();
                if (test.getID() == rule.getID()) {
                    it.set(rule);
                }
            }
        }
    }

    @Override
    public void saveReport(Report report) {
        if (report.getID().isNull()) {
            reports.add(report);
            report.getID().updateFromDB(rules.size());
        } else {
            for (ListIterator<Report> it = reports.listIterator(); it.hasNext();) {
                Report test = it.next();
                if (test.getID() == report.getID()) {
                    it.set(report);
                }
            }
            reports.add(report);
        }
    }

    @Override
    public void deletePayee(ID payeeID) throws IOException {
        for (Iterator<Payee> it = payees.iterator(); it.hasNext();) {
            Payee p = it.next();

            if (p.getID() == payeeID)
                it.remove();
        }

        for (Iterator<MatcherRule> it = rules.iterator(); it.hasNext();) {
            MatcherRule rule = it.next();

            if (rule.getPayee().getID() == payeeID)
                it.remove();
        }

        for (Iterator<Report> it = reports.iterator(); it.hasNext();) {
            Report report = it.next();

            if (report.getPayee().getID() == payeeID)
                it.remove();
        }
    }

    @Override
    public void deleteRule(ID ruleID) throws IOException {
        for (Iterator<MatcherRule> it = rules.iterator(); it.hasNext();) {
            MatcherRule r = it.next();

            if (r.getID() == ruleID)
                it.remove();
        }
    }


    @Override
    public void wipe() throws IOException {
        payees.clear();
        rules.clear();
        reports.clear();
    }

    @Override
    public String toString() {
        return "MemoryDatabaseImpl{" + "payees=(" + payees + "), rules=(" + rules + "), reports=(" + reports + ")}";
    }
}
