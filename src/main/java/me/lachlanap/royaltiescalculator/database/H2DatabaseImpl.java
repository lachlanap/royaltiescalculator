package me.lachlanap.royaltiescalculator.database;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.sql.*;
import java.util.*;
import java.util.regex.Pattern;
import me.lachlanap.royaltiescalculator.ApplicationInfo;
import me.lachlanap.royaltiescalculator.data.*;
import me.lachlanap.royaltiescalculator.data.Payee.PaymentMode;
import org.h2.tools.RunScript;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

/**
 *
 * @author lachlan
 */
class H2DatabaseImpl implements DatabaseImpl {

    private static final int VERSION = 4;
    private static final String DB_URL = "jdbc:h2:%s";
    private static final String DB_FILENAME = "royalties-db-V%d";
    private static final String SQL_INSTALL_SCRIPT_URL = "/install.sql";
    private static final DateTimeFormatter DATE_FORMAT = ISODateTimeFormat.basicDate();

    private Connection connection;
    private boolean newVersion;

    @Override
    public void init() throws IOException {
        newVersion = false;

        connectToDB();
        checkAndInstallDB();
    }

    @Override
    public boolean isNewVersion() {
        return newVersion;
    }

    @Override
    public void dispose() throws IOException {
        try {
            connection.close();
        } catch (SQLException sqle) {
            throw new IOException(sqle);
        }
    }

    private void connectToDB() throws IOException {
        try {
            connection = DriverManager.getConnection(getJdbcDatabaseUrl());
        } catch (SQLException sqle) {
            throw new IOException(sqle);
        }
    }

    private String getJdbcDatabaseUrl() {
        return String.format(
                DB_URL,
                ApplicationInfo.get().getWorkingDirectory()
                .resolve(String.format(DB_FILENAME, VERSION))
                .toAbsolutePath()
                .toString());
    }

    private void checkAndInstallDB() throws IOException {
        try (Statement statement = connection.createStatement()) {
            int tableCount;
            try (ResultSet set = statement.executeQuery("SHOW TABLES;")) {
                tableCount = set.last() ? set.getRow() : 0;
            }

            if (tableCount == 0) {
                try (Reader reader = new BufferedReader(new InputStreamReader(
                        H2DatabaseImpl.class.getResourceAsStream(SQL_INSTALL_SCRIPT_URL), StandardCharsets.UTF_8))) {
                    RunScript.execute(connection, reader);
                    newVersion = true;
                }
            }
        } catch (SQLException sqle) {
            throw new IOException(sqle);
        }
    }

    @Override
    public List<Payee> getPayees() throws IOException {
        try (Statement statement = connection.createStatement();
             ResultSet payeeSet = statement.executeQuery(
                     "SELECT * FROM PAYEE;")) {
            List<Payee> payees = new ArrayList<>();

            while (payeeSet.next()) {
                Payee p = payeeFromRow(payeeSet);
                payees.add(p);
            }

            return payees;
        } catch (SQLException sqle) {
            throw new IOException(sqle);
        }
    }

    @Override
    public List<MatcherRule> getMatcherRules() throws IOException {
        try (Statement statement = connection.createStatement()) {
            Map<Integer, Payee> payees = new HashMap<>();
            List<MatcherRule> rules = new ArrayList<>();

            try (ResultSet payeeSet = statement.executeQuery(
                    "SELECT * FROM PAYEE")) {
                while (payeeSet.next()) {
                    Payee p = payeeFromRow(payeeSet);
                    payees.put(p.getID().asInt(), p);
                }
            }

            try (ResultSet ruleSet = statement.executeQuery(
                    "SELECT * FROM MATCHER_RULE")) {
                while (ruleSet.next()) {
                    MatcherRule rule = ruleFromRow(ruleSet, payees);
                    rules.add(rule);
                }
            }

            return rules;
        } catch (SQLException sqle) {
            throw new IOException(sqle);
        }
    }

    @Override
    public List<Report> getReports(Period period) throws IOException {
        try (Statement statement = connection.createStatement()) {
            Map<Integer, Payee> payees = new HashMap<>();
            Set<Report> reports = new HashSet<>();

            try (ResultSet payeeSet = statement.executeQuery(
                    "SELECT * FROM PAYEE")) {
                while (payeeSet.next()) {
                    Payee p = payeeFromRow(payeeSet);
                    payees.put(p.getID().asInt(), p);
                }
            }

            try (PreparedStatement prepared = connection.prepareStatement(
                    "SELECT * FROM REPORT "
                    + "WHERE `PERIOD_START` = ?")) {
                prepared.setString(1, period.getStart().toString(DATE_FORMAT));

                try (ResultSet reportSet = prepared.executeQuery()) {
                    while (reportSet.next()) {
                        Report report = reportFromRow(reportSet, payees);
                        reports.remove(report);
                        reports.add(report);
                    }
                }
            }

            return new ArrayList<>(reports);
        } catch (SQLException sqle) {
            throw new IOException(sqle);
        }
    }

    @Override
    public List<Report> getReports() throws IOException {
        try (Statement statement = connection.createStatement()) {
            Map<Integer, Payee> payees = new HashMap<>();
            List<Report> reports = new ArrayList<>();

            try (ResultSet payeeSet = statement.executeQuery(
                    "SELECT * FROM PAYEE")) {
                while (payeeSet.next()) {
                    Payee p = payeeFromRow(payeeSet);
                    payees.put(p.getID().asInt(), p);
                }
            }

            try (ResultSet reportSet = statement.executeQuery(
                    "SELECT * FROM REPORT")) {
                while (reportSet.next()) {
                    Report report = reportFromRow(reportSet, payees);
                    reports.add(report);
                }
            }

            return reports;
        } catch (SQLException sqle) {
            throw new IOException(sqle);
        }
    }

    @Override
    public void savePayee(Payee payee) throws IOException {
        if (payee.getID().isNull())
            insertNewPayee(payee);
        else
            updateExistingPayee(payee);
    }

    private void insertNewPayee(Payee payee) throws IOException {
        String sql = "INSERT INTO `PAYEE` "
                     + "(`NAME`, `ADDRESS`, `EMAIL_ADDRESS`, `COMPANY`, `PAYMENT_MODE`, `GST_NEEDED`) VALUES "
                     + "(?, ?, ?, ?, ?, ?)";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, payee.getName());
            statement.setString(2, payee.getAddress());
            statement.setString(3, payee.getEmailAddress());
            statement.setString(4, payee.getCompany());
            statement.setInt(5, payee.getPaymentMode().ordinal());
            statement.setBoolean(6, payee.isGSTNeeded());

            statement.execute();

            try (ResultSet idSet = statement.getGeneratedKeys()) {
                if (idSet.next()) {
                    payee.getID().updateFromDB(idFrom(idSet));
                }
            }
        } catch (SQLException sqle) {
            throw new IOException(sqle);
        }
    }

    private void updateExistingPayee(Payee payee) throws IOException {
        String sql = "UPDATE `PAYEE` SET "
                     + "`NAME` = ?, `ADDRESS` = ?, `EMAIL_ADDRESS` = ?, "
                     + "`COMPANY` = ?, `PAYMENT_MODE` = ?, `GST_NEEDED` = ? "
                     + "WHERE ID = ?";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, payee.getName());
            statement.setString(2, payee.getAddress());
            statement.setString(3, payee.getEmailAddress());
            statement.setString(4, payee.getCompany());
            statement.setInt(5, payee.getPaymentMode().ordinal());
            statement.setBoolean(6, payee.isGSTNeeded());

            statement.setInt(7, payee.getID().asInt());

            statement.execute();
        } catch (SQLException sqle) {
            throw new IOException(sqle);
        }
    }

    @Override
    public void saveRule(MatcherRule rule) throws IOException {
        if (rule.getID().isNull())
            insertNewRule(rule);
        else
            updateExistingRule(rule);

    }

    private void insertNewRule(MatcherRule rule) throws IOException {
        String sql = "INSERT INTO `MATCHER_RULE` "
                     + "(`PATTERN`, `PAYEE_ID`, `PERCENT_ROYALTY`, `RRP`) VALUES "
                     + "(?, ?, ?, ?)";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, rule.getPattern());
            statement.setInt(2, rule.getPayee().getID().asInt());
            statement.setInt(3, rule.getPercentRoyalty());
            statement.setDouble(4, rule.getRRP());

            statement.execute();
        } catch (SQLException sqle) {
            throw new IOException(sqle);
        }
    }

    private void updateExistingRule(MatcherRule rule) throws IOException {
        String sql = "UPDATE `MATCHER_RULE` SET "
                     + "`PATTERN` = ?, `PAYEE_ID` = ?, `PERCENT_ROYALTY` = ?, `RRP` = ? "
                     + "WHERE ID = ?";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, rule.getPattern());
            statement.setInt(2, rule.getPayee().getID().asInt());
            statement.setInt(3, rule.getPercentRoyalty());
            statement.setDouble(4, rule.getRRP());

            statement.setInt(5, rule.getID().asInt());

            statement.execute();
        } catch (SQLException sqle) {
            throw new IOException(sqle);
        }
    }

    @Override
    public void saveReport(Report report) throws IOException {
        String sql = "INSERT INTO `REPORT` "
                     + "(`PAYEE_ID`, `PERIOD_START`, `REPORT_PATH`) VALUES "
                     + "(?, ?, ?)";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, report.getPayee().getID().asInt());
            statement.setString(2, report.getPeriod().getStart().toString(DATE_FORMAT));
            statement.setString(3, report.getReportFilePath().toString());
            statement.executeUpdate();
        } catch (SQLException sqle) {
            throw new IOException(sqle);
        }
    }

    @Override
    public void deletePayee(ID payeeID) throws IOException {
        try (PreparedStatement statement = connection.prepareStatement(
                "DELETE FROM `MATCHER_RULE` WHERE PAYEE_ID = ?")) {
            statement.setInt(1, payeeID.asInt());
            statement.executeUpdate();
        } catch (SQLException sqle) {
            throw new IOException(sqle);
        }

        try (PreparedStatement statement = connection.prepareStatement(
                "DELETE FROM `REPORT` WHERE PAYEE_ID = ?")) {
            statement.setInt(1, payeeID.asInt());
            statement.executeUpdate();
        } catch (SQLException sqle) {
            throw new IOException(sqle);
        }

        try (PreparedStatement statement = connection.prepareStatement(
                "DELETE FROM `PAYEE` WHERE ID = ?")) {
            statement.setInt(1, payeeID.asInt());
            statement.executeUpdate();
        } catch (SQLException sqle) {
            throw new IOException(sqle);
        }
    }

    @Override
    public void deleteRule(ID ruleID) throws IOException {
        try (PreparedStatement statement = connection.prepareStatement(
                "DELETE FROM `MATCHER_RULE` WHERE ID = ?")) {
            statement.setInt(1, ruleID.asInt());
            statement.executeUpdate();
        } catch (SQLException sqle) {
            throw new IOException(sqle);
        }
    }

    @Override
    public void wipe() throws IOException {
        try (Statement statement = connection.createStatement()) {
            statement.execute("DELETE FROM `MATCHER_RULE`");
            statement.execute("DELETE FROM `REPORT`");
            statement.execute("DELETE FROM `PAYEE`");
        } catch (SQLException sqle) {
            throw new IOException(sqle);
        }
    }

    private Payee payeeFromRow(ResultSet set) throws SQLException {
        return new Payee(idFrom(set),
                         set.getString("NAME"),
                         set.getString("ADDRESS"),
                         set.getString("EMAIL_ADDRESS"),
                         set.getString("COMPANY"),
                         PaymentMode.values()[set.getInt("PAYMENT_MODE")],
                         set.getBoolean("GST_NEEDED"));
    }

    private MatcherRule ruleFromRow(ResultSet set, Map<Integer, Payee> payees) throws SQLException {
        return new MatcherRule(idFrom(set),
                               set.getString("PATTERN"),
                               payees.get(set.getInt("PAYEE_ID")),
                               set.getInt("PERCENT_ROYALTY"),
                               set.getDouble("RRP"));
    }

    private Report reportFromRow(ResultSet set, Map<Integer, Payee> payees) throws SQLException {
        String reportPath = set.getString("REPORT_PATH");
        if (reportPath.charAt(1) == ':') {
            // If a Windows path
            reportPath = reportPath.substring(2)
                    .replaceAll(Pattern.quote("\\"), "/");
        }

        return new Report(idFrom(set),
                          payees.get(set.getInt("PAYEE_ID")),
                          Period.forDate(DATE_FORMAT.parseLocalDate(set.getString("PERIOD_START"))),
                          Paths.get(reportPath));
    }

    /**
     * Obtains the id from a result set entry. The id must be the first column.
     */
    private ID idFrom(ResultSet set) throws SQLException {
        return ID.fromInt(set.getInt(1));
    }

}
