package me.lachlanap.royaltiescalculator.database;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.*;
import me.lachlanap.royaltiescalculator.ApplicationInfo;
import me.lachlanap.royaltiescalculator.data.*;
import me.lachlanap.royaltiescalculator.export.Exporter;
import me.lachlanap.royaltiescalculator.export.Importer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A facade for interacting with an arbitrary datastore.
 *
 * With the exception of {@link #init()}, {@link Database} is entirely asyncronous, having its own single-thread
 * executor.
 */
public class Database {

    private static final Logger LOG = LoggerFactory.getLogger(Database.class);
    private static int LOG_ID = 0;

    /**
     * Creates a mock database. Can have test data if needed.
     */
    public static Database newMockDatabase(boolean testDataIncluded) {
        return new Database(new MemoryDatabaseImpl(testDataIncluded ? 1 : 0));
    }

    public static Database newIntensiveMockDatabase() {
        return new Database(new MemoryDatabaseImpl(2));
    }

    public static Database newStressMockDatabase() {
        return new Database(new MemoryDatabaseImpl(3));
    }


    public static Database newStandardDatabase() {
        return new Database(new H2DatabaseImpl());
    }

    private final ExecutorService executor;
    private final DatabaseImpl impl;
    private final CountDownLatch initLatch;
    private boolean initialised;

    private Database(DatabaseImpl impl) {
        this.impl = impl;
        executor = Executors.newSingleThreadExecutor();
        initLatch = new CountDownLatch(1);
    }

    public void init() throws IOException {
        LOG.info("{} Database Init; impl = {}", lid(), impl.getClass());

        impl.init();
        initialised = true;
        initLatch.countDown();
    }

    public boolean isNewVersion() {
        LOG.info("{} isNewVersion = {}", lid(), impl.isNewVersion());

        checkInit();
        return impl.isNewVersion();
    }

    public void dispose() throws IOException {
        impl.dispose();
    }

    private void checkInit() {
        try {
            if (!initLatch.await(10, TimeUnit.SECONDS)) {
                throw new IllegalStateException("Timeout waiting for database to be initialised");
            }
        } catch (InterruptedException ie) {
            throw new IllegalStateException("Interrupted while waiting for database to be initialised", ie);
        }

        if (!initialised)
            throw new IllegalStateException("Database has not been initialised");
    }

    @Override
    public String toString() {
        return "Database{" + impl + "}";
    }

    public Future<List<MatcherRule>> getMatcherRules() {
        LOG.info("{} getMatcherRules", lid());

        return executor.submit(l(new Callable<List<MatcherRule>>() {

            @Override
            public List<MatcherRule> call() throws Exception {
                checkInit();
                return impl.getMatcherRules();
            }

        }));
    }

    public Future<List<Payee>> getPayees() {
        LOG.info("{} getPayees", lid());

        return executor.submit(l(new Callable<List<Payee>>() {

            @Override
            public List<Payee> call() throws Exception {
                checkInit();
                return impl.getPayees();
            }

        }));
    }

    public Future<List<Report>> getReports(final Period period) {
        LOG.info("{} getReports({})", lid(), period);

        return executor.submit(l(new Callable<List<Report>>() {

            @Override
            public List<Report> call() throws Exception {
                checkInit();
                List<Report> reports = impl.getReports(period);
                return reports;
            }

        }));
    }

    public Future<List<Report>> getReports() {
        LOG.info("{} getReports()", lid());

        return executor.submit(l(new Callable<List<Report>>() {

            @Override
            public List<Report> call() throws Exception {
                checkInit();
                List<Report> reports = impl.getReports();
                return reports;
            }

        }));
    }

    public void savePayee(final Payee payee) {
        LOG.info("{} savePayee({})", lid(), payee);

        executor.submit(l(new Callable<Void>() {

            @Override
            public Void call() throws Exception {
                checkInit();
                impl.savePayee(payee);

                return null;
            }

        }));
    }

    public void savePayees(List<Payee> payees) {
        for (Payee payee : payees)
            savePayee(payee);
    }

    public void saveRule(final MatcherRule rule) {
        LOG.info("{} saveRule({})", new Object[]{lid(), rule});

        executor.submit(l(new Callable<Void>() {

            @Override
            public Void call() throws Exception {
                checkInit();
                impl.saveRule(rule);
                return null;
            }

        }));
    }

    public void saveRules(final List<MatcherRule> rules) {
        for (MatcherRule rule : rules)
            saveRule(rule);
    }

    public void saveReport(final Report report) {
        LOG.info("{} saveReport({})", lid(), report);

        executor.submit(l(new Callable<Void>() {

            @Override
            public Void call() throws Exception {
                checkInit();
                impl.saveReport(report);
                return null;
            }

        }));
    }

    public void deletePayee(final Payee payee) {
        deletePayee(payee.getID());
    }

    public void deletePayee(final ID payee) {
        LOG.info("{} deletePayee({})", lid(), payee);

        executor.submit(l(new Callable<Void>() {

            @Override
            public Void call() throws Exception {
                checkInit();
                impl.deletePayee(payee);
                return null;
            }

        }));
    }

    public void deleteRule(final MatcherRule rule) {
        deletePayee(rule.getID());
    }

    public void deleteRule(final ID ruleID) {
        LOG.info("{} deleteRule({})", lid(), ruleID);

        executor.submit(l(new Callable<Void>() {

            @Override
            public Void call() throws Exception {
                checkInit();
                impl.deleteRule(ruleID);
                return null;
            }

        }));
    }

    public void wipe() {
        LOG.warn("{} wipe()", new Object[]{lid()});

        executor.submit(l(new Callable<Void>() {

            @Override
            public Void call() throws Exception {
                checkInit();
                impl.wipe();
                return null;
            }

        }));
    }

    private synchronized static int lid() {
        return LOG_ID++;
    }

    public void autoExport() throws IOException {
        Path autoExportFile = ApplicationInfo.get().getWorkingDirectory()
                .resolve("auto-export.json");

        new Exporter().exportData(this, autoExportFile);
    }

    public void importFromLastAutoExport() throws IOException {
        Path autoExportFile = ApplicationInfo.get().getWorkingDirectory()
                .resolve("auto-export.json");

        if (Files.exists(autoExportFile)) {
            new Importer().importData(this, autoExportFile);
        }
    }

    private static class LoggingCallable<T> implements Callable<T> {

        private final Callable<T> callable;

        public LoggingCallable(Callable<T> callable) {
            this.callable = callable;
        }

        @Override
        public T call() throws Exception {
            try {
                return callable.call();
            } catch (Exception e) {
                LOG.error("Error running database operation", e);
                throw e;
            }
        }
    }

    private static <T> Callable<T> l(Callable<T> c) {
        return new LoggingCallable<>(c);
    }
}
