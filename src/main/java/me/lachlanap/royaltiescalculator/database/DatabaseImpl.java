package me.lachlanap.royaltiescalculator.database;

import java.io.IOException;
import java.util.List;
import me.lachlanap.royaltiescalculator.data.*;

/**
 *
 * @author lachlan
 */
interface DatabaseImpl {

    /**
     * Initialises the database. Can setup db connections or open files.
     */
    public void init() throws IOException;

    /**
     * Checks if a new version of the db schema was installed. Implying it was a new version. Call only after init().
     */
    public boolean isNewVersion();

    /**
     * Initialises the database. Can setup db connections or open files.
     */
    public void dispose() throws IOException;

    /**
     * Gets all the matching rules in the system.
     */
    public List<MatcherRule> getMatcherRules() throws IOException;

    /**
     * Gets all the payees in the system.
     */
    public List<Payee> getPayees() throws IOException;

    /**
     * Gets all reports for a particular period.
     */
    public List<Report> getReports(Period period) throws IOException;

    /**
     * Gets all reports.
     */
    public List<Report> getReports() throws IOException;

    /**
     * Saves either a new payee or updates an existing one.
     */
    public void savePayee(Payee payee) throws IOException;

    /**
     * Saves a matching rule. Payee must exist.
     */
    public void saveRule(MatcherRule rule) throws IOException;

    /**
     * Saves a report in the database.
     */
    public void saveReport(Report report) throws IOException;

    public void deletePayee(ID payee) throws IOException;

    public void deleteRule(ID ruleID) throws IOException;

    /**
     * Completely erases all data from the database.
     */
    public void wipe() throws IOException;
}
