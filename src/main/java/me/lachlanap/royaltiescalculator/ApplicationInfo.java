/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.lachlanap.royaltiescalculator;

import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import me.lachlanap.royaltiescalculator.gui.Messaging;

/**
 *
 * @author Lachlan Phillips
 */
public class ApplicationInfo {

    private final Path workingDirectory;

    public ApplicationInfo() {
        try {
            Path executable = Paths.get(getClass().getProtectionDomain().getCodeSource().getLocation().toURI());

            if (executable.toString().endsWith(".jar"))
                workingDirectory = executable.getParent();
            else if (Files.isDirectory(executable))
                workingDirectory = executable.getParent().getParent();
            else
                throw new RuntimeException("Not running from a jar or a directory");
        } catch (URISyntaxException use) {
            Messaging.exception(use, "Error deciding on a good working directory", null);
            throw new RuntimeException(use);
        }
    }

    public Path getWorkingDirectory() {
        return workingDirectory;
    }

    public static String getApplicationName() {
        return "Royalties Calculator";
    }

    private static ApplicationInfo INSTANCE;

    public static ApplicationInfo get() {
        if (INSTANCE == null)
            INSTANCE = new ApplicationInfo();
        return INSTANCE;
    }
}
