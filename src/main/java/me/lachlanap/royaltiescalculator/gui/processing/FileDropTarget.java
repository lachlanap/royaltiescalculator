package me.lachlanap.royaltiescalculator.gui.processing;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;
import me.lachlanap.royaltiescalculator.gui.Messaging;

/**
 *
 * @author lachlan
 */
class FileDropTarget extends DropTarget {

    private final ProcessingGUI outer;

    FileDropTarget(final ProcessingGUI outer) {
        this.outer = outer;
    }

    @Override
    public synchronized void drop(DropTargetDropEvent evt) {
        if (outer.isProcessing() || !evt.getTransferable().isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
            evt.rejectDrop();
        } else {
            try {
                evt.acceptDrop(DnDConstants.ACTION_COPY);
                List<File> droppedFiles = (List<File>) evt.getTransferable().
                        getTransferData(DataFlavor.javaFileListFlavor);
                for (File file : droppedFiles) {
                    outer.handleFile(file);
                }
            } catch (UnsupportedFlavorException | IOException ex) {
                Messaging.exception(ex, "Could not process dropped file", outer);
            }
        }
    }

    @Override
    public synchronized void dragEnter(DropTargetDragEvent dtde) {
        super.dragOver(dtde);
        if (!outer.isProcessing() && dtde.getTransferable().isDataFlavorSupported(DataFlavor.javaFileListFlavor))
            dtde.acceptDrag(DnDConstants.ACTION_COPY);
        else
            dtde.rejectDrag();
    }

    @Override
    public synchronized void dragOver(DropTargetDragEvent dtde) {
        super.dragOver(dtde);
        if (!outer.isProcessing() && dtde.getTransferable().isDataFlavorSupported(DataFlavor.javaFileListFlavor))
            dtde.acceptDrag(DnDConstants.ACTION_COPY);
        else
            dtde.rejectDrag();
    }

}
