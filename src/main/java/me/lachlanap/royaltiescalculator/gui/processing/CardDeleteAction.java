package me.lachlanap.royaltiescalculator.gui.processing;

/**
 *
 * @author lachlan
 */
public interface CardDeleteAction {

    public void onDelete(DataFileCard dfc);
}
