package me.lachlanap.royaltiescalculator.gui.processing;

import java.util.List;
import me.lachlanap.royaltiescalculator.data.Period;
import me.lachlanap.royaltiescalculator.processing.matcher.PayeeMatcherResults;
import me.lachlanap.royaltiescalculator.processing.report.ReportsGenerator;

/**
 *
 * @author lachlan
 */
class ReportGeneratorTask implements Runnable {

    private final PayeeMatcherResults payeeMatcherResults;
    private final ProcessingGUI outer;
    private final Period period;

    public ReportGeneratorTask(PayeeMatcherResults payeeMatcherResults, final ProcessingGUI outer, Period period) {
        this.outer = outer;
        this.payeeMatcherResults = payeeMatcherResults;
        this.period = period;
    }

    @Override
    public void run() {
        if (payeeMatcherResults.getPayeeResults().isEmpty())
            outer.reportGenerationFailed(new IllegalArgumentException("No reports to generate"));

        final ReportsGenerator reportsGenerator = new ReportsGenerator(payeeMatcherResults, outer.getDatabase(), period);

        reportsGenerator.setUpdateRunnable(new Runnable() {

            @Override
            public void run() {
                outer.reportGenerationUpdate(reportsGenerator.getDone(), reportsGenerator.getTotal());
            }
        });

        try {
            List<ReportsGenerator.GeneratorResult> results = reportsGenerator.generateReports();
            outer.reportGenerationFinished(results);
        } catch (RuntimeException e) {
            outer.reportGenerationFailed(e);
        }
    }
}
