package me.lachlanap.royaltiescalculator.gui.report;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.IOException;
import javax.print.PrintService;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Chromaticity;
import javax.print.attribute.standard.MediaName;
import javax.swing.JButton;
import javax.swing.SwingWorker;
import me.lachlanap.royaltiescalculator.AsyncServices;
import me.lachlanap.royaltiescalculator.data.Report;
import me.lachlanap.royaltiescalculator.gui.Messaging;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPageable;

/**
 *
 * @author lachlan
 */
class PrintReportActionListener implements ActionListener {

    private static PrintRequestAttributeSet previousPrintingAttributes;
    private static PrintService previousPrintService;
    private final Report report;

    public PrintReportActionListener(Report report) {
        this.report = report;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        final JButton source = (JButton) e.getSource();
        source.setEnabled(false);

        AsyncServices.getInstance().execute(new SwingWorker() {
            @Override
            protected Void doInBackground() throws Exception {
                try {
                    doPrint();
                } catch (IOException | PrinterException ex) {
                    Messaging.exception(ex, "Error printing report. "
                                            + "Please go to "
                                            + report.getReportFilePath().toString()
                                            + " relative to the root of the Royalties Calculator installation.",
                                        null);
                }
                return null;
            }

            @Override
            protected void done() {
                source.setEnabled(true);
            }
        });
    }

    private void doPrint() throws IOException, PrinterException, NullPointerException, IllegalArgumentException,
                                  HeadlessException {
        PDDocument document = PDDocument.load(report.getReportFilePath().toFile());
        try {
            PrinterJob printJob = PrinterJob.getPrinterJob();
            printJob.setJobName("Royalties Report for " + report.getPayee().getNameOrCompany());
            printJob.setPageable(new PDPageable(document, printJob));
            PrintRequestAttributeSet properties;
            if (previousPrintingAttributes != null) {
                properties = previousPrintingAttributes;
                printJob.setPrintService(previousPrintService);
            } else {
                properties = new HashPrintRequestAttributeSet();
                properties.add(MediaName.ISO_A4_WHITE);
                properties.add(Chromaticity.MONOCHROME);
                if (!printJob.printDialog(properties))
                    return;
                previousPrintingAttributes = properties;
                previousPrintService = printJob.getPrintService();
            }
            printJob.print(properties);
        } finally {
            if (document != null) {
                document.close();
            }
        }
    }

}
