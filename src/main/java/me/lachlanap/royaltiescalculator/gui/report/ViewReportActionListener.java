package me.lachlanap.royaltiescalculator.gui.report;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import me.lachlanap.royaltiescalculator.data.Report;
import me.lachlanap.royaltiescalculator.gui.Messaging;

/**
 *
 * @author lachlan
 */
class ViewReportActionListener implements ActionListener {

    private final Report report;

    public ViewReportActionListener(Report report) {
        this.report = report;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (!Desktop.isDesktopSupported()) {
            Messaging.error("Opening of files is not supported by Java on this OS. "
                            + "Please go to "
                            + report.getReportFilePath().toString()
                            + " relative to the root of the Royalties Calculator installation.",
                            null);
        } else {
            try {
                Desktop.getDesktop().open(report.getReportFilePath().toFile());
            } catch (IOException ioe) {
                Messaging.exception(ioe, "Error opening report. "
                                         + "Please go to "
                                         + report.getReportFilePath().toString()
                                         + " relative to the root of the Royalties Calculator installation.",
                                    null);
            }
        }
    }

}
