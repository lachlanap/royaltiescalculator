package me.lachlanap.royaltiescalculator.gui.report;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.SwingWorker;
import me.lachlanap.royaltiescalculator.AsyncServices;
import me.lachlanap.royaltiescalculator.data.Report;
import me.lachlanap.royaltiescalculator.gui.Messaging;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author lachlan
 */
class EmailReportActionListener implements ActionListener {

    private static final DateTimeFormatter YMD = DateTimeFormat.forPattern("yyyy-MM-dd");
    private static final DateTimeFormatter DMY = DateTimeFormat.forPattern("dd MMM yyyy");

    private final Report report;

    public EmailReportActionListener(Report report) {
        this.report = report;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        final JButton source = (JButton) e.getSource();
        source.setEnabled(false);

        AsyncServices.getInstance().execute(new SwingWorker() {
            @Override
            protected Void doInBackground() throws Exception {
                try {
                    sendEmail();
                } catch (EmailException ex) {
                    Messaging.exception(ex, "Failed to email report", null);
                }
                return null;
            }

            @Override
            protected void done() {
                source.setEnabled(true);
            }
        });
    }

    private void sendEmail() throws EmailException {
        String filename = "royalties-report-" + report.getPeriod().getEnd().toString(YMD) + ".pdf";
        String description = "Royalties Report for " + report.getPayee().getNameOrCompany() + " for the period ending "
                             + report.getPeriod().getEnd().toString(DMY);

        EmailAttachment attachment = new EmailAttachment();

        attachment.setPath(report.getReportFilePath().toAbsolutePath().toString());
        attachment.setDisposition(EmailAttachment.ATTACHMENT);
        attachment.setName(filename);
        attachment.setDescription(description);

        MultiPartEmail email = new MultiPartEmail();
        email.setHostName("mail.internode.on.net");
        email.setSmtpPort(25);
        email.setFrom("no-reply@gould.com.au");
        email.setSubject(description);
        email.addReplyTo("stephen@gould.com.au");
        email.addTo(report.getPayee().getEmailAddress());
        email.setMsg("A Royalties Report for the period ending "
                     + report.getPeriod().getEnd().toString(DMY));

        email.attach(attachment);

        email.send();
    }

}
