/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.lachlanap.royaltiescalculator.gui;

import javax.swing.DefaultComboBoxModel;
import me.lachlanap.royaltiescalculator.data.Period;

/**
 *
 * @author Lachlan Phillips
 */
public class PeriodComboBoxModel extends DefaultComboBoxModel<Period> {

    public PeriodComboBoxModel(int howManyToShow) {
        Period tmp = Period.getCurrentPeriod();
        for (int i = 0; i < howManyToShow; i++) {
            addElement(tmp);
            tmp = tmp.previous();
        }
    }

}
