/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.lachlanap.royaltiescalculator.gui.rules;

import me.lachlanap.royaltiescalculator.data.ID;
import me.lachlanap.royaltiescalculator.data.MatcherRule;
import me.lachlanap.royaltiescalculator.database.Database;

/**
 *
 * @author Lachlan Phillips
 */
class MatcherRuleWrapper {

    private final ID id;
    private final PayeeWrapper payee;
    private String pattern;
    private int percentRoyalty;
    private double rrp;

    private boolean touched;

    public MatcherRuleWrapper(PayeeWrapper payee, MatcherRule rule) {
        id = rule.getID();
        this.payee = payee;
        pattern = rule.getPattern();
        percentRoyalty = rule.getPercentRoyalty();
        rrp = rule.getRRP();
    }

    public MatcherRuleWrapper(PayeeWrapper payee, String pattern, int percentRoyalty) {
        id = ID.nullID();
        this.payee = payee;
        this.pattern = pattern;
        this.percentRoyalty = percentRoyalty;
        rrp = 0;
    }

    @Override
    public String toString() {
        return percentRoyalty + "% : " + pattern;
    }

    public void setPattern(String pattern) {
        pattern = pattern.trim();
        if (pattern.isEmpty())
            return;
        if (this.pattern.equals(pattern))
            return;
        this.pattern = pattern;
        touched = true;
    }

    public void setPercentRoyalty(int percent) {
        if (this.percentRoyalty == percent)
            return;
        this.percentRoyalty = percent;
        touched = true;
    }

    public void setRRP(double rrp) {
        if (this.rrp == rrp)
            return;
        this.rrp = rrp;
        touched = true;
    }

    public void update(Database database) {
        if (touched) {
            System.out.println(">>> " + id + " " + pattern);
            database.saveRule(toRule());
            touched = false;
        }
    }

    public MatcherRule toRule() {
        return new MatcherRule(id, pattern, payee.toPayee(), percentRoyalty, rrp);
    }

    public ID getId() {
        return id;
    }

    public PayeeWrapper getPayee() {
        return payee;
    }

    public String getPattern() {
        return pattern;
    }

    public int getPercentRoyalty() {
        return percentRoyalty;
    }

    public double getRrp() {
        return rrp;
    }

    public void setRrp(double rrp) {
        this.rrp = rrp;
    }
}
