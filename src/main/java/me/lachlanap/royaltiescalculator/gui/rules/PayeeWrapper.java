/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.lachlanap.royaltiescalculator.gui.rules;

import me.lachlanap.royaltiescalculator.data.ID;
import me.lachlanap.royaltiescalculator.data.Payee;
import me.lachlanap.royaltiescalculator.database.Database;

/**
 *
 * @author Lachlan Phillips
 */
class PayeeWrapper {

    private final ID id;
    private String name;
    private String address;
    private String emailAddress;
    private String company;
    private boolean useRRP;
    private boolean includeGST;

    private boolean touched;

    public PayeeWrapper() {
        id = ID.nullID();
        name = "Untitled";
        address = "";
        emailAddress = "email@example.com";
        company = "";
        useRRP = false;
        includeGST = false;
    }

    public PayeeWrapper(Payee payee) {
        id = payee.getID();
        name = payee.getName();
        address = payee.getAddress();
        emailAddress = payee.getEmailAddress();
        company = payee.getCompany();
        useRRP = payee.getPaymentMode() == Payee.PaymentMode.RRP;
        includeGST = payee.isGSTNeeded();

        System.out.println("<<< " + id + " " + name + " " + includeGST);
    }

    public void update(Database database) {
        if (touched) {
            System.out.println(">>> " + id + " " + name + " " + includeGST);
            database.savePayee(toPayee());
            touched = false;
        }
    }

    public Payee toPayee() {
        return new Payee(id,
                         name, address, emailAddress, company,
                         useRRP ? Payee.PaymentMode.RRP : Payee.PaymentMode.Sales,
                         includeGST);
    }

    @Override
    public String toString() {
        if (name.isEmpty())
            return company;
        else
            return name;
    }

    public ID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (this.name.equals(name))
            return;
        this.name = name;
        touched = true;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        if (this.address.equals(address))
            return;
        this.address = address;
        touched = true;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        if (this.emailAddress.equals(emailAddress))
            return;
        this.emailAddress = emailAddress;
        touched = true;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        if (this.company.equals(company))
            return;
        this.company = company;
        touched = true;
    }

    public boolean isUseRRP() {
        return useRRP;
    }

    public void setUseRRP(boolean useRRP) {
        if (this.useRRP == useRRP)
            return;
        this.useRRP = useRRP;
        touched = true;
    }

    public boolean isIncludeGST() {
        return includeGST;
    }

    public void setIncludeGST(boolean includeGST) {
        if (this.includeGST == includeGST)
            return;
        this.includeGST = includeGST;
        touched = true;
    }
}
