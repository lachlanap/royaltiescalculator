/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.lachlanap.royaltiescalculator.gui;

import java.awt.HeadlessException;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import me.lachlanap.royaltiescalculator.ApplicationInfo;

/**
 *
 * @author Lachlan Phillips
 */
public class Messaging {

    public static void exception(Exception exception, JComponent parentComponent) throws HeadlessException {
        exception.printStackTrace();
        showErrorDialog(parentComponent, exception.toString());
    }

    public static void exception(Exception exception, String message, JComponent parentComponent) throws HeadlessException {
        exception.printStackTrace();
        showErrorDialog(parentComponent, message + ":\n" + exception);
    }

    public static void error(String message, JComponent parentComponent) throws HeadlessException {
        showErrorDialog(parentComponent, message);
    }

    private static void showErrorDialog(final JComponent parentComponent, final String text) throws HeadlessException {
        if (SwingUtilities.isEventDispatchThread())
            showErrorDialogOnEventThread(parentComponent, text);
        else {
            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    showErrorDialogOnEventThread(parentComponent, text);
                }
            });
        }
    }

    private static void showErrorDialogOnEventThread(JComponent parentComponent, String text) throws HeadlessException {
        JOptionPane.showMessageDialog(parentComponent,
                                      formatMessage(text),
                                      ApplicationInfo.getApplicationName(),
                                      JOptionPane.ERROR_MESSAGE);
    }

    public static boolean askYesNo(final String message) throws InterruptedException, InvocationTargetException {
        if (SwingUtilities.isEventDispatchThread())
            return showYesNoDialogOnEventThread(message);
        else {
            final AtomicBoolean result = new AtomicBoolean(false);
            SwingUtilities.invokeAndWait(new Runnable() {

                @Override
                public void run() {
                    result.set(showYesNoDialogOnEventThread(message));
                }
            });
            return result.get();
        }
    }

    private static boolean showYesNoDialogOnEventThread(String message) throws HeadlessException {
        int result = JOptionPane.showConfirmDialog(null,
                                                   formatMessage(message),
                                                   ApplicationInfo.getApplicationName(),
                                                   JOptionPane.YES_NO_OPTION);
        return result == JOptionPane.YES_OPTION;
    }

    private static String formatMessage(String message) {
        return "<html><body><p style='width: 400px'>"
               + message.replaceAll("\n", "<br>")
               + "</p></body></html>";
    }
}
