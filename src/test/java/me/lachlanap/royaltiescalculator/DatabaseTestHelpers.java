package me.lachlanap.royaltiescalculator;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.Future;
import me.lachlanap.royaltiescalculator.data.MatcherRule;
import me.lachlanap.royaltiescalculator.data.Payee;
import me.lachlanap.royaltiescalculator.data.Report;
import me.lachlanap.royaltiescalculator.database.Database;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import static me.lachlanap.royaltiescalculator.BasicTestHelpers.g;
import static org.hamcrest.CoreMatchers.hasItem;

/**
 *
 * @author lachlan
 */
public class DatabaseTestHelpers {

    public static Database blankDatabase() {
        Database database = Database.newMockDatabase(false);
        try {
            database.init();
        } catch (IOException ex) {
            throw new RuntimeException("Could not initialise the database for test", ex);
        }
        return database;
    }

    public static Database databaseWith(Payee... payees) {
        Database database = Database.newMockDatabase(false);
        try {
            database.init();
        } catch (IOException ex) {
            throw new RuntimeException("Could not initialise the database for test");
        }

        database.savePayees(Arrays.asList(payees));

        return database;
    }

    public static Database databaseWith(MatcherRule... rules) {
        Database database = Database.newMockDatabase(false);
        try {
            database.init();
        } catch (IOException ex) {
            throw new RuntimeException("Could not initialise the database for test");
        }

        for (MatcherRule rule : rules)
            database.savePayee(rule.getPayee());

        database.saveRules(Arrays.asList(rules));

        return database;
    }

    public static Database databaseWith(Report... reports) {
        Database database = Database.newMockDatabase(false);
        try {
            database.init();
        } catch (IOException ex) {
            throw new RuntimeException("Could not initialise the database for test");
        }

        for (Report r : reports) {
            database.savePayee(r.getPayee());
            database.saveReport(r);
        }

        return database;
    }

    public static Matcher<Future<? extends Iterable<? super Report>>> hasReport(final Report check) {
        return futureMatcher(hasItem(check));
    }

    public static Matcher<Future<? extends Iterable<? super Report>>> notHasReport(final Report check) {
        return notFutureMatcher(hasItem(check));
    }

    public static <T> Matcher<Future<? extends T>> notFutureMatcher(final Matcher<T> matcher) {
        return new TypeSafeMatcher<Future<? extends T>>() {

            @Override
            public boolean matchesSafely(Future<? extends T> item) {
                return !matcher.matches(g(item));
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("not a future of ")
                        .appendDescriptionOf(matcher);
            }

            @Override
            protected void describeMismatchSafely(Future<? extends T> item, Description description) {
                description.appendText("was ").appendValue(g(item));
            }
        };
    }

    public static <T> Matcher<Future<? extends T>> futureMatcher(final Matcher<T> matcher) {
        return new TypeSafeMatcher<Future<? extends T>>() {

            @Override
            public boolean matchesSafely(Future<? extends T> item) {
                return matcher.matches(g(item));
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("a future of ")
                        .appendDescriptionOf(matcher);
            }

            @Override
            protected void describeMismatchSafely(Future<? extends T> item, Description description) {
                description.appendText("was ").appendValue(g(item));
            }
        };
    }
}
