package me.lachlanap.royaltiescalculator;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import me.lachlanap.royaltiescalculator.data.*;
import me.lachlanap.royaltiescalculator.data.Payee.PaymentMode;

/**
 *
 * @author lachlan
 */
public class BasicTestHelpers {

    /**
     * Extracts the result from the future, in a RuntimeException sort of way.
     */
    public static <T> T g(Future<T> future) {
        try {
            return future.get();
        } catch (InterruptedException | ExecutionException ex) {
            throw new RuntimeException("Could not extract result from future", ex);
        }
    }

    public static Period period(int year, int quarter) {
        return new Period(year, Quarter.valueOf("Q" + quarter));
    }

    public static Payee payeeWithName(String name) {
        return Payee.fromName(name);
    }

    public static Payee payeeWithNameAndCompany(String name, String company) {
        return new Payee(name, "", "", company, PaymentMode.Sales, false);
    }

    public static MatcherRule ruleFor(String payee, String pattern, int percentRoyalty) {
        return new MatcherRule(pattern, payeeWithName(payee), percentRoyalty, 0);
    }

    public static MatcherRule ruleFor(Payee payee, String pattern, int percentRoyalty) {
        return new MatcherRule(pattern, payee, percentRoyalty, 0);
    }

    public static Report reportForPeriod(String name, Period period) {
        return new Report(payeeWithName(name), period, null);
    }

    public static Report reportForPeriod(Payee payee, Period period) {
        return new Report(payee, period, null);
    }

    public static Report reportForPeriod(Payee payee, Period period, final String name) {
        return new Report(payee, period, null) {

            @Override
            public String toString() {
                return name;
            }

        };
    }
}
