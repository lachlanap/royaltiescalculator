package me.lachlanap.royaltiescalculator.processing.matcher;

import java.util.concurrent.ExecutionException;
import me.lachlanap.royaltiescalculator.BasicTestHelpers;
import me.lachlanap.royaltiescalculator.DatabaseTestHelpers;
import me.lachlanap.royaltiescalculator.data.MatcherRule;
import me.lachlanap.royaltiescalculator.processing.SalesData;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

/**
 *
 * @author lachlan
 */
public class PayeeMatcherTest {

    @Test
    public void testBasicMatching() throws ExecutionException, InterruptedException {
        MatcherRule rule = BasicTestHelpers.ruleFor("Someone", "^1", 10);

        PayeeMatcher matcher = new PayeeMatcher(DatabaseTestHelpers.databaseWith(rule));

        SalesData.Item item = new SalesData.Item("1", "An Item", 1, 1);
        SalesData data = new SalesData().addItem(item);

        PayeeMatcherResults results = matcher.match(data);

        assertThat(results.getPayeeResults().get(0).getPayee().getName(),
                   is("Someone"));
        assertThat(results.getPayeeResults().get(0).getItems().get(0),
                   is(new PayeeMatcherResults.Item(item, 10, 0)));
    }

    @Test
    public void testNotMatching() throws ExecutionException, InterruptedException {
        MatcherRule rule = BasicTestHelpers.ruleFor("Someone", "^2", 10);

        PayeeMatcher matcher = new PayeeMatcher(DatabaseTestHelpers.databaseWith(rule));

        SalesData.Item item = new SalesData.Item("1", "An Item", 1, 1);
        SalesData data = new SalesData().addItem(item);

        PayeeMatcherResults results = matcher.match(data);

        assertThat(results.getPayeeResults().get(0).getPayee().getName(),
                   is("Someone"));
        assertThat(results.getPayeeResults().get(0).getItems().size(), is(0));
    }

    @Test
    public void testMultiMatching() throws ExecutionException, InterruptedException {
        MatcherRule rule1 = BasicTestHelpers.ruleFor("Author", "^1", 10);
        MatcherRule rule2 = BasicTestHelpers.ruleFor("Coauthor", "^1", 10);

        PayeeMatcher matcher = new PayeeMatcher(DatabaseTestHelpers.databaseWith(rule1, rule2));

        SalesData.Item item = new SalesData.Item("1", "An Item", 1, 1);
        SalesData data = new SalesData().addItem(item);

        PayeeMatcherResults results = matcher.match(data);

        assertThat(results.getPayeeResults().get(0).getPayee().getName(),
                   is("Author"));
        assertThat(results.getPayeeResults().get(0).getItems().get(0),
                   is(new PayeeMatcherResults.Item(item, 10, 0)));

        assertThat(results.getPayeeResults().get(1).getPayee().getName(),
                   is("Coauthor"));
        assertThat(results.getPayeeResults().get(1).getItems().get(0),
                   is(new PayeeMatcherResults.Item(item, 10, 0)));
    }
}
