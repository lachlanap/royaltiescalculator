package me.lachlanap.royaltiescalculator.processing.parser;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import me.lachlanap.royaltiescalculator.processing.SalesData;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

/**
 *
 * @author lachlan
 */
public class ExternalACSVParserTest {

    @Test
    public void testParseMany() throws URISyntaxException, IOException {
        SalesData expected = new SalesData()
                .addItem(new SalesData.Item("UTP0001r", "Your Family History Archives, ", 1, 41.25))
                .addItem(new SalesData.Item("UTP0002r", "What was the Voyage Really Like", 1, 41.25));

        DataFile dataFile = new DataFile(
                Paths.get(ExternalACSVParserTest.class.getResource("/externala/test1.csv").toURI()),
                DataFile.Format.ExternalACSV);

        SalesDataReader parser = new SalesDataReader();
        SalesData parsedSalesData
                = parser.parse(Arrays.asList(new DataFile[]{dataFile}));

        assertThat(parsedSalesData, is(expected));
    }

    @Test
    public void testParseDuplicates() throws URISyntaxException, IOException {
        SalesData expected = new SalesData()
                .addItem(new SalesData.Item("UTP0281r", "Discover Scottish Church Records", 80, 1294.4));

        DataFile dataFile1 = new DataFile(
                Paths.get(ExternalACSVParserTest.class.getResource("/externala/test3_1.csv").toURI()),
                DataFile.Format.ExternalACSV);
        DataFile dataFile2 = new DataFile(
                Paths.get(ExternalACSVParserTest.class.getResource("/externala/test3_2.csv").toURI()),
                DataFile.Format.ExternalACSV);

        SalesDataReader parser = new SalesDataReader();
        SalesData parsedSalesData
                = parser.parse(Arrays.asList(new DataFile[]{dataFile1, dataFile2}));

        assertThat(parsedSalesData, is(expected));
    }

    @Test
    public void doesntDieAfterBlank() throws URISyntaxException, IOException {
        SalesData expected = new SalesData()
                .addItem(new SalesData.Item("UTP0001r", "Your Family History Archives, ", 1, 8.25))
                .addItem(new SalesData.Item("UTP0002r", "What was the Voyage Really Like", 1, 8.25))
                .addItem(new SalesData.Item("UTP0004r", "Family and Local History Resources", 1, 18.95));

        DataFile dataFile = new DataFile(
                Paths.get(ExternalACSVParserTest.class.getResource("/externala/test2.csv").toURI()),
                DataFile.Format.ExternalACSV);

        SalesDataReader parser = new SalesDataReader();
        SalesData parsedSalesData
                = parser.parse(Arrays.asList(new DataFile[]{dataFile}));

        assertThat(parsedSalesData, is(expected));
    }

    @Test
    public void testMatchesCorrectFormat() throws URISyntaxException, IOException {
        Path file = Paths.get(PastelCSVParserTest.class
                .getResource("/externala/test1.csv").toURI());
        Parser parser = new ExternalACSVParser();

        assertThat(parser.fileLooksLike(file), is(true));
    }

    @Test
    public void testDoesntMatchInvalidFormat() throws URISyntaxException, IOException {
        Path file = Paths.get(PastelCSVParserTest.class
                .getResource("/magento/basicsales.csv").toURI());
        Parser parser = new ExternalACSVParser();

        assertThat(parser.fileLooksLike(file), is(false));
    }
}
