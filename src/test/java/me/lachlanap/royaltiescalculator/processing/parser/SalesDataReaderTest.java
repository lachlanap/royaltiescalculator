package me.lachlanap.royaltiescalculator.processing.parser;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import me.lachlanap.royaltiescalculator.processing.SalesData;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

/**
 *
 * @author lachlan
 */
public class SalesDataReaderTest {

    @Test
    public void testBasicSalesData() throws URISyntaxException, IOException {
        /* Expected */
        SalesData expected = new SalesData()
                .addItem(new SalesData.Item("AUE0001", "Ebook 1", 335, 0));

        /* Setup */
        DataFile dataFile = new DataFile(Paths.get(SalesDataReaderTest.class.getResource("/magento/basicsales.csv").
                toURI()),
                                         DataFile.Format.MagentoCSV);

        SalesDataReader parser = new SalesDataReader();
        SalesData parsedSalesData
                = parser.parse(Arrays.asList(new DataFile[]{dataFile}));

        /* Assert */
        assertThat(parsedSalesData, is(expected));
    }

    @Test
    public void testGuessingMagentoFormat() throws URISyntaxException, IOException {
        Path path = Paths.get(SalesDataReaderTest.class.getResource("/magento/basicsales.csv").toURI());
        SalesDataReader sdr = new SalesDataReader();

        assertThat(sdr.guessFormatOf(path), is(DataFile.Format.MagentoCSV));
    }

    @Test
    public void testGuessingPastelFormat() throws URISyntaxException, IOException {
        Path path = Paths.get(SalesDataReaderTest.class.getResource("/pastel/basicsales.csv").toURI());
        SalesDataReader sdr = new SalesDataReader();

        assertThat(sdr.guessFormatOf(path), is(DataFile.Format.PastelCSV));
    }

}
