package me.lachlanap.royaltiescalculator.processing.parser;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import me.lachlanap.royaltiescalculator.processing.SalesData;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

/**
 *
 * @author lachlan
 */
public class MagentoCSVParserTest {

    private static final DataFile.Format NULL_FORMAT = DataFile.Format.MagentoCSV;

    @Test(expected = FileNotFoundException.class)
    public void testNoSuchFile() throws IOException {
        DataFile dataFile = new DataFile(Paths.get("no", "such", "path"), NULL_FORMAT);

        SalesDataReader parser = new SalesDataReader();
        parser.parse(Arrays.asList(new DataFile[]{dataFile}));
    }

    @Test
    public void testParseBasic() throws URISyntaxException, IOException {
        /* Expected */
        SalesData expected = new SalesData()
                .addItem(new SalesData.Item("AUE0001", "Ebook 1", 335, 0));

        /* Setup */
        DataFile dataFile = new DataFile(Paths.get(MagentoCSVParserTest.class
                .getResource("/magento/basicsales.csv").
                toURI()),
                                         DataFile.Format.MagentoCSV);

        SalesDataReader parser = new SalesDataReader();
        SalesData parsedSalesData
                = parser.parse(Arrays.asList(new DataFile[]{dataFile}));

        /* Assert */
        assertThat(parsedSalesData, is(expected));
    }

    @Test
    public void testParseBasicWithoutQuotes() throws URISyntaxException, IOException {
        /* Expected */
        SalesData expected = new SalesData()
                .addItem(new SalesData.Item("AUE0001", "Ebook 1", 335, 0));

        /* Setup */
        DataFile dataFile = new DataFile(Paths.get(MagentoCSVParserTest.class
                .getResource("/magento/basicsales_noquotes.csv").
                toURI()),
                                         DataFile.Format.MagentoCSV);

        SalesDataReader parser = new SalesDataReader();
        SalesData parsedSalesData
                = parser.parse(Arrays.asList(new DataFile[]{dataFile}));

        /* Assert */
        assertThat(parsedSalesData, is(expected));
    }

    @Test
    public void testParseMany() throws URISyntaxException, IOException {
        /* Expected */
        SalesData expected = new SalesData()
                .addItem(new SalesData.Item("AUE0001", "Ebook 1", 335, 0))
                .addItem(new SalesData.Item("AUE0002", "Ebook 2", 123, 34))
                .addItem(new SalesData.Item("AUE0004", "Ebook 4", 43, 89))
                .addItem(new SalesData.Item("AUE0003", "Ebook 3", 12, 20));

        /* Setup */
        DataFile dataFile = new DataFile(Paths.get(MagentoCSVParserTest.class
                .getResource("/magento/manysales.csv").
                toURI()),
                                         DataFile.Format.MagentoCSV);

        SalesDataReader parser = new SalesDataReader();
        SalesData parsedSalesData
                = parser.parse(Arrays.asList(new DataFile[]{dataFile}));

        /* Assert */
        assertThat(parsedSalesData, is(expected));
    }

    @Test
    public void testMatchesCorrectFormat() throws URISyntaxException, IOException {
        Path file = Paths.get(PastelCSVParserTest.class
                .getResource("/magento/basicsales.csv").toURI());
        Parser parser = new MagentoCSVParser();

        assertThat(parser.fileLooksLike(file), is(true));
    }

    @Test
    public void testMatchesCorrectFormatWithoutQuotes() throws URISyntaxException, IOException {
        Path file = Paths.get(PastelCSVParserTest.class
                .getResource("/magento/basicsales_noquotes.csv").toURI());
        Parser parser = new MagentoCSVParser();

        assertThat(parser.fileLooksLike(file), is(true));
    }

    @Test
    public void testDoesntMatcheInvalidFormat() throws URISyntaxException, IOException {
        Path file = Paths.get(PastelCSVParserTest.class
                .getResource("/pastel/basicsales.csv").toURI());
        Parser parser = new MagentoCSVParser();

        assertThat(parser.fileLooksLike(file), is(false));
    }
}
