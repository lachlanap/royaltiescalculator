package me.lachlanap.royaltiescalculator.processing.parser;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import me.lachlanap.royaltiescalculator.processing.SalesData;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

/**
 *
 * @author lachlan
 */
public class PastelCSVParserTest {

    @Test(expected = FileNotFoundException.class)
    public void testNoSuchFile() throws IOException {
        Path file = Paths.get("no", "such", "path");

        Parser parser = new PastelCSVParser();
        parser.parse(file);
    }

    @Test
    public void testParseBasic() throws URISyntaxException, IOException {
        /* Expected */
        SalesData expected = new SalesData()
                .addItem(new SalesData.Item("AUE0001", "Ebook 1", 335, 0));

        /* Setup */
        Path file = Paths.get(PastelCSVParserTest.class.getResource("/pastel/basicsales.csv").toURI());

        Parser parser = new PastelCSVParser();
        SalesData parsedSalesData = parser.parse(file);

        /* Assert */
        assertThat(parsedSalesData, is(expected));
    }

    @Test
    public void testParseMany() throws URISyntaxException, IOException {
        /* Expected */
        SalesData expected = new SalesData()
                .addItem(new SalesData.Item("AUE0001", "Ebook 1", 335, 0))
                .addItem(new SalesData.Item("AUE0002", "Ebook 2", 123, 34))
                .addItem(new SalesData.Item("AUE0004", "Ebook 4", 43, 89))
                .addItem(new SalesData.Item("AUE0003", "Ebook 3", 12, 20));

        /* Setup */
        Path file = Paths.get(PastelCSVParserTest.class.getResource("/pastel/manysales.csv").toURI());

        Parser parser = new PastelCSVParser();
        SalesData parsedSalesData = parser.parse(file);

        /* Assert */
        assertThat(parsedSalesData, is(expected));
    }

    @Test
    public void testMatchesCorrectFormat() throws URISyntaxException, IOException {
        Path file = Paths.get(PastelCSVParserTest.class.getResource("/pastel/basicsales.csv").toURI());
        Parser parser = new PastelCSVParser();

        assertThat(parser.fileLooksLike(file), is(true));
    }

    @Test
    public void testDoesntMatchesInvalidFormat() throws URISyntaxException, IOException {
        Path file = Paths.get(PastelCSVParserTest.class.getResource("/magento/basicsales.csv").toURI());
        Parser parser = new PastelCSVParser();

        assertThat(parser.fileLooksLike(file), is(false));
    }
}
