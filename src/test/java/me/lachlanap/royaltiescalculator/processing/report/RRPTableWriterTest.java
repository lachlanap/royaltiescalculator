package me.lachlanap.royaltiescalculator.processing.report;

import me.lachlanap.royaltiescalculator.processing.SalesData;
import me.lachlanap.royaltiescalculator.processing.matcher.PayeeMatcherResults;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

/**
 *
 * @author lachlan
 */
public class RRPTableWriterTest {

    @Test
    public void testBasicItem() {
        PayeeMatcherResults.Item resultItem = new PayeeMatcherResults.Item(
                new SalesData.Item("AU0001", "A Book", 2, 0), 20, 5);
        RRPTableWriter tableWriter = new RRPTableWriter();

        assertThat(tableWriter.makeRow(resultItem), is(new String[]{
            "A Book", "2", "$1.00", "20%", "$5.00", "$2.00"
        }));
    }

    @Test
    public void testFooterWithTwoItems() {
        PayeeMatcherResults.Item resultItem1 = new PayeeMatcherResults.Item(
                new SalesData.Item("AU0001", "A Book", 2, 0), 20, 5);
        PayeeMatcherResults.Item resultItem2 = new PayeeMatcherResults.Item(
                new SalesData.Item("AU0002", "Another Book", 7, 0), 10, 5);

        RRPTableWriter tableWriter = new RRPTableWriter();
        tableWriter.makeRow(resultItem1);
        tableWriter.makeRow(resultItem2);

        assertThat(tableWriter.makeFooter(), is(new String[][]{
            {"Total", "", "", "", "", "$5.50"}
        }));
        assertThat(tableWriter.getTotal(), is(5.50));
    }

    @Test
    public void testFooterWithTwoItemsAndGST() {
        PayeeMatcherResults.Item resultItem1 = new PayeeMatcherResults.Item(
                new SalesData.Item("AU0001", "A Book", 2, 0), 20, 5);
        PayeeMatcherResults.Item resultItem2 = new PayeeMatcherResults.Item(
                new SalesData.Item("AU0002", "Another Book", 7, 0), 10, 5);

        RRPTableWriter tableWriter = new RRPTableWriter();
        tableWriter.includeGST();
        tableWriter.makeRow(resultItem1);
        tableWriter.makeRow(resultItem2);

        assertThat(tableWriter.makeFooter(), is(new String[][]{
            {"Includes GST", "", "", "", "", "$0.50"},
            {"Total", "", "", "", "", "$5.50"}
        }));
        assertThat(tableWriter.getTotal(), is(5.50));
    }
}
