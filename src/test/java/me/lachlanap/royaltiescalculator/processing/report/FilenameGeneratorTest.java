/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.lachlanap.royaltiescalculator.processing.report;

import me.lachlanap.royaltiescalculator.data.Period;
import me.lachlanap.royaltiescalculator.data.Quarter;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 *
 * @author lachlan
 */
public class FilenameGeneratorTest {

    @Test
    public void basicReport() {
        FilenameGenerator generator = new FilenameGenerator();

        String result = generator.generateNameFor(new Period(2014, Quarter.Q2),
                                                  "Test Person");

        assertThat(result, is("2014-Q2-royalties-test-person.pdf"));
    }

    @Test
    public void doesNotHaveDuplicateDashes() {
        FilenameGenerator generator = new FilenameGenerator();

        String result = generator.generateNameFor(new Period(2014, Quarter.Q2),
                                                  "Test Person - Type 3");

        assertThat(result, is("2014-Q2-royalties-test-person-type-3.pdf"));
    }
}
