package me.lachlanap.royaltiescalculator.processing.report;

import me.lachlanap.royaltiescalculator.processing.SalesData;
import me.lachlanap.royaltiescalculator.processing.matcher.PayeeMatcherResults;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

/**
 *
 * @author lachlan
 */
public class SalesTableWriterTest {

    @Test
    public void testBasicItem() {
        PayeeMatcherResults.Item resultItem = new PayeeMatcherResults.Item(
                new SalesData.Item("AU0001", "A Book", 2, 10d), 20, 0);
        SalesTableWriter tableWriter = new SalesTableWriter();

        assertThat(tableWriter.makeRow(resultItem), is(new String[]{
            "A Book", "2", "$1.00", "20%", "$10.00", "$2.00"
        }));
    }

    @Test
    public void testFooterWithTwoItems() {
        PayeeMatcherResults.Item resultItem1 = new PayeeMatcherResults.Item(
                new SalesData.Item("AU0001", "A Book", 2, 10d), 20, 0);
        PayeeMatcherResults.Item resultItem2 = new PayeeMatcherResults.Item(
                new SalesData.Item("AU0002", "Another Book", 4, 20d), 10, 0);

        SalesTableWriter tableWriter = new SalesTableWriter();
        tableWriter.makeRow(resultItem1);
        tableWriter.makeRow(resultItem2);

        assertThat(tableWriter.makeFooter(), is(new String[][]{
            {"Total", "", "", "", "", "$4.00"}
        }));
        assertThat(tableWriter.getTotal(), is(4.00));
    }

    @Test
    public void testFooterWithTwoItemsAndGST() {
        PayeeMatcherResults.Item resultItem1 = new PayeeMatcherResults.Item(
                new SalesData.Item("AU0001", "A Book", 2, 10d), 20, 0);
        PayeeMatcherResults.Item resultItem2 = new PayeeMatcherResults.Item(
                new SalesData.Item("AU0002", "Another Book", 4, 20d), 10, 0);

        SalesTableWriter tableWriter = new SalesTableWriter();
        tableWriter.includeGST();
        tableWriter.makeRow(resultItem1);
        tableWriter.makeRow(resultItem2);

        assertThat(tableWriter.makeFooter(), is(new String[][]{
            {"Subtotal", "", "", "", "", "$4.00"},
            {"GST", "", "", "", "", "$0.40"},
            {"Total", "", "", "", "", "$4.40"}
        }));
        assertThat(tableWriter.getTotal(), is(4.40));
    }
}
