package me.lachlanap.royaltiescalculator.processing;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

/**
 *
 * @author lachlan
 */
public class SalesDataTest {

    @Test
    public void testAppending() {
        SalesData data1 = new SalesData()
                .addItem(new SalesData.Item("item1", "item 1", 0, 0));

        SalesData data2 = new SalesData()
                .addItem(new SalesData.Item("item2", "item 2", 0, 0));

        SalesData expected = new SalesData()
                .addItem(new SalesData.Item("item1", "item 1", 0, 0))
                .addItem(new SalesData.Item("item2", "item 2", 0, 0));

        assertThat(data1.append(data2), is(expected));
    }

    @Test
    public void appendingDuplicatesMergesItems() {
        SalesData data1 = new SalesData()
                .addItem(new SalesData.Item("item1", "item 1", 2, 6.5));

        SalesData data2 = new SalesData()
                .addItem(new SalesData.Item("item1", "item 1", 1, 1))
                .addItem(new SalesData.Item("item2", "item 2", 1, 1));

        SalesData expected = new SalesData()
                .addItem(new SalesData.Item("item1", "item 1", 3, 7.5))
                .addItem(new SalesData.Item("item2", "item 2", 1, 1));

        assertThat(data1.append(data2), is(expected));
    }
}
