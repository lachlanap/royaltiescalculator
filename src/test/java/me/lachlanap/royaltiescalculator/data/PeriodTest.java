/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.lachlanap.royaltiescalculator.data;

import org.joda.time.LocalDate;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 *
 * @author Lachlan Phillips
 */
public class PeriodTest {

    @Test
    public void aPeriodOfQ3HasAPreviousOfQ2() {
        Period period = new Period(2014, Quarter.Q3);
        Period previous = period.previous();

        assertThat(previous.getYear(), is(2014));
        assertThat(previous.getQuarter(), is(Quarter.Q2));
    }

    @Test
    public void aPeriodOfQ1HasAPreviousOfQ4AndGoesBackAYear() {
        Period period = new Period(2014, Quarter.Q1);
        Period previous = period.previous();

        assertThat(previous.getYear(), is(2013));
        assertThat(previous.getQuarter(), is(Quarter.Q4));
    }


    @Test
    public void rendersMonthRangeCorrectly() {
        Period period = new Period(2014, Quarter.Q1);

        assertThat(period.toMonthRangeString(), is("01 Jan 2014 - 31 Mar 2014"));
    }


    @Test
    public void periodForDateReturnsCorrectForStartOfAPeriod() {
        Period period = Period.forDate(new LocalDate(2014, 04, 01));

        assertThat(period.getYear(), is(2014));
        assertThat(period.getQuarter(), is(Quarter.Q2));
    }

    @Test
    public void periodForDateReturnsCorrectForMiddleOfAPeriod() {
        Period period = Period.forDate(new LocalDate(2014, 05, 13));

        assertThat(period.getYear(), is(2014));
        assertThat(period.getQuarter(), is(Quarter.Q2));
    }

    @Test
    public void periodForDateReturnsCorrectForEndOfAPeriod() {
        Period period = Period.forDate(new LocalDate(2014, 06, 30));

        assertThat(period.getYear(), is(2014));
        assertThat(period.getQuarter(), is(Quarter.Q2));
    }
}
