/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.lachlanap.royaltiescalculator.data;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Lachlan Phillips
 */
public class QuarterTest {

    @Test
    public void quarterFromMonthIsCorrectForQ1() {
        Assert.assertThat(Quarter.fromMonth(1), CoreMatchers.is(Quarter.Q1));
        Assert.assertThat(Quarter.fromMonth(3), CoreMatchers.is(Quarter.Q1));
    }

    @Test
    public void quarterFromMonthIsCorrectForQ4() {
        Assert.assertThat(Quarter.fromMonth(10), CoreMatchers.is(Quarter.Q4));
        Assert.assertThat(Quarter.fromMonth(12), CoreMatchers.is(Quarter.Q4));
    }
}
