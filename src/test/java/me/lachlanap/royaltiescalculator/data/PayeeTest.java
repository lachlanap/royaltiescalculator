package me.lachlanap.royaltiescalculator.data;

import org.junit.Test;

import static me.lachlanap.royaltiescalculator.BasicTestHelpers.payeeWithName;
import static me.lachlanap.royaltiescalculator.BasicTestHelpers.payeeWithNameAndCompany;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

/**
 *
 * @author lachlan
 */
public class PayeeTest {

    @Test
    public void testGetFirstnameFromTwoNames() {
        Payee p = payeeWithName("First Last");

        assertThat(p.getFirstname(), is("First"));
    }

    @Test
    public void testGetFirstnameFromOneName() {
        Payee p = payeeWithName("Single");

        assertThat(p.getFirstname(), is("Single"));
    }

    @Test
    public void nameOrCompanyReturnsNameWhenThereIsOne() {
        Payee p = payeeWithNameAndCompany("Single", "");

        assertThat(p.getNameOrCompany(), is("Single"));
    }

    @Test
    public void nameOrCompanyReturnsCompanyWhenThereIsNoName() {
        Payee p = payeeWithNameAndCompany("", "Company Name");

        assertThat(p.getNameOrCompany(), is("Company Name"));
    }
}
