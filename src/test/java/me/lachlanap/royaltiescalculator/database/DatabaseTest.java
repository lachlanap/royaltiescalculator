package me.lachlanap.royaltiescalculator.database;

import java.util.Arrays;
import java.util.List;
import me.lachlanap.royaltiescalculator.data.*;
import org.junit.Test;

import static me.lachlanap.royaltiescalculator.BasicTestHelpers.*;
import static me.lachlanap.royaltiescalculator.DatabaseTestHelpers.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

/**
 *
 * @author lachlan
 */
public class DatabaseTest {

    @Test
    public void testCanAddAPayee() {
        Database db = blankDatabase();

        Payee payee = payeeWithName("Test");
        db.savePayee(payee);

        assertThat(g(db.getPayees()).get(0), is(payee));
    }

    @Test
    public void testCanAddSeveralPayees() {
        Database db = blankDatabase();

        Payee payee1 = payeeWithName("Test 1");
        Payee payee2 = payeeWithName("Test 2");
        db.savePayees(Arrays.asList(new Payee[]{payee1, payee2}));

        assertThat(g(db.getPayees()).get(0), is(payee1));
        assertThat(g(db.getPayees()).get(1), is(payee2));
    }

    @Test
    public void testRemovingPayeeDoesntShowAgain() {
        Payee p1 = payeeWithName("Test 1");
        Payee p2 = payeeWithName("Test 2");
        Database db = databaseWith(p1, p2);

        db.deletePayee(p1);

        assertThat(g(db.getPayees()).get(0), is(p2));
    }

    @Test
    public void removingPayeeDeletesRulesForThatPayee() {
        Payee p1 = payeeWithName("Test 1");
        MatcherRule rule = ruleFor(p1, "^AU0", 20);
        Database db = databaseWith(rule);

        db.deletePayee(p1);

        assertThat(g(db.getMatcherRules()).size(), is(0));
    }

    @Test
    public void removingPayeeDeletesReportsForThatPayee() {
        Payee p1 = payeeWithName("Test 1");
        Report report = reportForPeriod(p1, period(2014, 1));
        Database db = databaseWith(report);

        db.deletePayee(p1);

        assertThat(g(db.getReports()).size(), is(0));
    }

    @Test
    public void testGetReportsInAPeriod() {
        Period firstPeriod = period(2014, 1);
        Period secondPeriod = period(2014, 2);

        Report r1 = reportForPeriod("P1", firstPeriod);
        Report r2 = reportForPeriod("P1", secondPeriod);
        Report r3 = reportForPeriod("P2", firstPeriod);

        Database db = databaseWith(r1,
                                   r2,
                                   r3);

        List<Report> reports = g(db.getReports(firstPeriod));
        assertThat(reports, hasItems(r1, r3));
    }

    @Test
    public void getReportsForAPeriodOnlyReturnsTheLatest() {
        Payee p = payeeWithName("Payee");
        Database db = databaseWith(p);
        Report r1 = reportForPeriod(p, period(2014, 1), "Report 1");
        Report r2 = reportForPeriod(p, period(2014, 1), "Report 2");

        db.saveReport(r1);
        db.saveReport(r2);

        assertThat(db.getReports(period(2014, 1)), futureMatcher(hasItem(sameInstance(r2))));
        assertThat(db.getReports(period(2014, 1)), notFutureMatcher(hasItem(sameInstance(r1))));
    }
}
